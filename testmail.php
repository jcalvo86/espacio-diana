<?php 
    ini_set( 'display_errors', 1 );
    error_reporting( E_ALL );
    $from = "contacto@espaciodiana.cl";
    $to = "jcalvo86@gmail.com";
    $subject = "Test Html";
    
    $headers = "De:" . $from;
    $headers .= "Return-Path: Espacio Diana <contacto@espaciodiana.cl>\r\n"; 
    $headers .= "Organization: Espacio Diana\r\n";
    $headers .= 'From: Boleteria Espacio Diana <boleteria@espaciodiana.cl>' . "\r\n";

    $headers .= "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    
    $message ='';

    $message .='<!DOCTYPE html>';
    $message .='<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">';
    $message .='<head>';

    $message .='<style>';
    $message .='p {color:white !important; }';
    $message .='.mail{display:flex; flex-flow:row; }';
    $message .='.image{background-size:cover; background-position:center; width:40%;}';
    $message .='.content{width:100%; display:block; min-height:400px; font-size:16px; background-color:black; color:white; padding:10px;}';
    $message .='a {text-decoration:none; font-weight:bold; color:black; background-color:white; border-radius:10px; padding:5px;}'; 
    $message .='tr{ border-style: solid; border-color: white; border-width: 1px;}';
    $message .='table{ border-style: solid; border-color: white; border-width: 1px; width: 50%; text-align: center; margin-bottom:20px;}';

    $message .='</style>';
    $message .='</head>';

    $message .='<body>';
    $message .='<div class="mail">';
    $message .='<div class="image" style="background-image:url(https://www.espaciodiana.cl/files/actividad/7/portada.jpg); "></div>';
    $message .='<div class="content">';

    $message .= "<h3>Entrada para OBRA </h3>";
    $message .= "<p>Hola USUARIO, tus entradas para OBRA en su función de  24 agosto / 14:00 horas, fueron adquiridas correctamente.</p>";

    $message .= '<table class="table">';
    $message .= '<thead><tr><th scope="col">Nombre</th><th scope="col">Código</th></tr></thead><tbody>';

    $message .= '<tr><td>nombre</td><td> 1234</td></tr>';
    $message .= '<tr><td>nombre</td><td> 1234</td></tr>';
    $message .= '<tr><td>nombre</td><td> 1234</td></tr>';

    $message .= '</tbody></table>';

    $message .= "<a href='www.espaciodiana.cl/funcion.php?funcion_id=0' class='btn btn-rojo btn-fullwidth'>Ver Obra</a>" ;
    
    $message .= "<p> La obra quedará habilitada para que puedas presenciarla desde XXXXX a XXXX horas del día de la función </p>" ;
    $message .= "<p> Ante cualquier duda puedes comunicarte al email contacto@espaciodiana.cl </p>" ;

    $message .= '</div>';
    $message .= '</div>';
    $message .='</body>';
    $message .='</html>';

    if (mail($to,$subject,$message, $headers)) {
        
        echo "Email successfully sent to ".$to;
    } else {
        echo "Email sending failed...";
    }
?>
