<?php   
    include 'inc/template/navbar.php';
    include 'inc/php/actividades_list.php';
    $actividades = getActividadesListProximos(3);


?>
<div class="contenedor">

    <!-- CABECERA - video y logo-->
    <div class="segment_cabecera_home background-image">

        <div class="segment_cabecera_contenedor">
            <div class="segment_cabecera_titulo" style="line-height: 65%;">
                <!-- <h1>ESPACIO DIANA, <br> <span class="text-red" style="font-size:50%;"> cada rincón es un viaje.</span></h1> -->
                <!-- <img src="files/logodiana_web_negro.png" alt="" style="width:100%;"> -->
                <video autoplay muted loop id="myVideo" style="z-index:-10;
                    min-width: 100%;
                    min-height: 100%;
                    width: auto;
                    height: auto;
                    position: fixed;    
                    top: 50%;
                    left: 50%;
                    transform: translateX(-50%) translateY(-50%); ">
                    <source src="files/intro_v2.mp4" type="video/mp4">
                </video>
            </div>

            <br>

            <!-- <div class="segment_cabecera_opciones">
                <div class="segment_cabecera_opciones_boton">
                    <button type="button" class="btn btn-secondary btn-lg btn-block">Conócenos</button>
                </div>
            
                <div class="segment_cabecera_opciones_texto">
                   <h4> Conoce lo que cada rincón de espacio diana tiene para ti.</h4>
                </div>                
            </div> -->
        </div>
    </div>

    <!-- POPUP COVID -->
    <!-- <div class="segment_proximos_title layout-box-fat" style="background-color: darkred; color:white; width:100% ; padding:2rem; text-align:center; position:relative; opacity: 80%;">
        <span id='close' onclick='this.parentNode.parentNode.removeChild(this.parentNode); return false;' style="position:absolute; top:2rem; right:2rem; font-size:2rem;"><i class="far fa-times-circle"></i></span>
        <p style="margin-bottom:0;">
            Informamos que  debido a la contingencia sanitaria nuestro centro cultural se encuentra cerrado para actividades con afluencia de público. 
        </p>
    </div> -->

    <!-- PARTES  DE NUESTRA WEB  cada grafica representar una parte del menu-->
    <div class="segment_historia flex-row layout-box-noheight background-negro" style=" width:100%">

        <!-- <div class="fondo">
            <div class="flex-3"></div>
            <div class="flex-2 background-rojo"></div>
        </div> -->
        <div class="flex-row flex-1  historia_prev_content">
            <div class="flex-col flex-1 flex-center" >
            <img id="header_image" src="files/imagenes/dianaactividades_0001.webp" alt="" style="width:100%;">            
            </div>
            <div class="flex-col  flex-1 layout-box-right" style="padding-top:0; padding-bottom:0;">
                <h2>
                    Espacio Diana es un lugar de 
                </h2>
                <h2 class="rojo" id="header_variable" style="font-size:5rem;">diversidad</h2>
            </div>

        </div>
    </div>

    <!-- SLIDER RECOMENDACIONES-->
    <?php $actividades_familiares = getActividadesListFamilia();?>
    <div class="segment_proximos_title layout-box-fat background-gris" style="width:100%">
        <h3>Ocurriendo en Espacio Diana:</h3>
        <div class="segment_proximos_eventos">

        <div id="carouselExampleControls" class="carousel slide carousel-fade" data-ride="carousel" style="width:100%;">
            <div class="carousel-inner" id="carousel_PEF">

                <div class="carousel-item active" style="padding: 0;">
                    <a class="card flex-1 background-image" href="cartelera.php" style="width:100%; background-image:url(files/actividad/diciembre2020/portada_h.jpg);">
                        <div class="card_image">
                            <div class="card_image_type"> Obras</div>
                        </div>

                        <div class="card_description flex-1" style="background-color:rgba(0,0,0,0.8)">
                            <h3>Cartelera Diciembre.</h3>
                            <p>Conoce las actividades que tenemos para ti durante diciembre.</p>

                            <p class="text-red">Ver más...</p>
                        </div>
                    </a>
                </div>


                <div class="carousel-item" style="padding: 0;">
                <a class="card flex-1 background-image" href="cartelera.php" style="width:100%; background-image:url(files/actividad/diciembre2020/portada_h2.jpg);">
                        <div class="card_image">
                            <div class="card_image_type"> Talleres</div>
                        </div>

                        <div class="card_description flex-1" style="background-color:rgba(0,0,0,0.8)">
                            <h3>Talleres infantiles</h3>
                            <p>Espacio Diana te invita a participar de talleres dirigidos a los más pequeños</p>

                            <p class="text-red">Ver más...</p>
                        </div>
                    </a>
                </div>

<!-- 
                <div class="carousel-item" style="padding: 0;">
                    <a class="card flex-1 background-image" href="actividad.php?id=<?php echo $actividad_fam['actividad_id']?>" style="width:100%; background-image:url(files/actividad/<?php echo $actividad_fam['actividad_id']?>/portada_h.jpg);">
                        <div class="card_image">
                            <div class="card_image_type"> <?php echo $actividad_fam['actividad_tipo']?> | <?php echo $actividad_fam['actividad_publico']?></div>
                        </div>

                        <div class="card_description flex-1" style="background-color:rgba(0,0,0,0.8)">
                            <h3><?php echo $actividad_fam['actividad_nombre']?></h3>
                            <p><?php echo text_capitalize($actividad_fam['actividad_tipo']) ." ". $actividad_fam['actividad_formato'] ." ". $actividad_fam['actividad_fechas'];?></p>
                            <p><?php echo text_trim($actividad_fam['actividad_descripcion'],150);?></p>
                            <p class="text-red">Ver más...</p>
                        </div>
                    </a>
                </div> -->

            </div>
        </div>




        </div>
    </div>

    <!-- PARTES  DE NUESTRA WEB  cada grafica representar una parte del menu-->
    <div class="segment_historia flex-row" style="background-color: black;">

        <div class="fondo">
            <div class="flex-3"></div>
            <div class="flex-2 background-rojo"></div>
        </div>
        <div class="flex-col flex-1 layout-box-left historia_prev_content">
            <h2>Espacio Diana</h2>
            <p>
                Es un lugar destinado en la cultura, ocio y entretención, situado en el corazón del Barrio San Diego, en un edificio patrimonial de principios del siglo XX y emplazado detrás del tradicional parque de entretenciones Juegos Diana.  Su origen está asociado a ese histórico  parque techado de juegos mecánicos, cuya presencia en la ciudad data de la década de los años 30 del siglo pasado y ocupa un lugar destacado en el imaginario de varias generaciones de santiaguinos.
            </p>

            <a href="nosotros.php" class="boton_segmento background-image" style="background-image:linear-gradient(rgba(0,0,0,0),rgba(0,0,0,1)),url(files/imagenes/dianafondo_0002.jpg)">
                <h3>Quienes Somos</h3>
            </a>
            <a href="cartelera.php" class="boton_segmento background-image" style="background-image:linear-gradient(rgba(0,0,0,0),rgba(0,0,0,1)),url(files/imagenes/dianafondo_0001.jpg)">
                <h3>Cartelera</h3>
            </a>

        </div>

        <div class="flex-col  layout-box-right flex-1">
            <div class="boton_segmento-spacer">
            </div>
            <a href="servicios.php" class="boton_segmento background-image" style="background-image:linear-gradient(rgba(0,0,0,0),rgba(0,0,0,1)),url(files/imagenes/dianafondo_0003.jpg)">
                <h3>Servicios</h3>
            </a>
            <a href="noticias.php" class="boton_segmento background-image" style="background-image:linear-gradient(rgba(0,0,0,0),rgba(0,0,0,1)),url(files/imagenes/dianafondo_0004.jpg)">
                <h3>Noticias</h3>
            </a>
        </div>
    </div>

    <!-- EN CARTELERA Teatro En cartelera-->
    <?php $actividades_cartelera = getActividadesListProximos(3,'obra', "next", "active");
    ?>
    <div class="segment_populares layout-box-fat" style="width: 100%; background-color: black;">
        <h3>En cartelera</h3>
        <hr>
        <div class="segment_proximos_eventos">

            <?php foreach($actividades_cartelera as $actividad_cartelera){ ?>
                <a class="card flex-1 background-image" href="actividad.php?id=<?php echo $actividad_cartelera['actividad_id']?>" style="background-image:url(files/actividad/<?php echo $actividad_cartelera['actividad_id']?>/portada.jpg); height:auto; background-color: transparent;;">
                    <div class="card_image">
                        <div class="card_image_type"> <?php echo $actividad_cartelera['actividad_tipo']?> | <?php echo $actividad_cartelera['actividad_publico']?></div>
                    </div>

                    <div class="card_description flex-1" style="background-color:rgba(0,0,0,0.8); padding: 1rem; marign:0; height: 100%;    flex-flow: column;
                    display: flex;
                    justify-content: space-around;">
                        <div>
                            <h3><?php echo $actividad_cartelera['actividad_nombre']?></h3>
                            <p class="text-grey text-small"><?php echo text_capitalize($actividad_cartelera['actividad_tipo']) ." ". $actividad_cartelera['actividad_formato'] ." ". $actividad_cartelera['actividad_fechas'];?></p>
                            <p><?php echo text_trim($actividad_cartelera['actividad_descripcion'],150);?></p>
                        </div>

                        <p class="text-red">Ver más...</p>
                    </div>
                </a>
            <?php } ?>

        </div>
    </div>

    <!-- PRÓXIMAMENTE-->
    <!-- <?php $actividades_proximas = getActividadesListProximos(3,'all', "next", "soon");
    $display = "";
    ?>
    <div class="segment_proximos_title layout-box-fat" style="background-color: black; width:100%" <?php echo $display?>>
        <h3>Próximos Estrenos</h3>
        <hr>
        <div class="segment_proximos_eventos">
            <?php foreach($actividades_proximas as $actividad_prox){ ?>
            <a class="card flex-30" href="actividad.php?id=<?php echo $actividad_prox['actividad_id']?>">
                <div class="card_image" style="background-image:url(files/actividad/<?php echo $actividad_prox['actividad_id']?>/portada.jpg);">
                    <div class="card_image_type"> <?php echo text_capitalize($actividad_prox['actividad_tipo'])?> </div>
                </div>

                <div class="card_description flex-1">
                    <h3><?php echo $actividad_prox['actividad_nombre']?></h3>
                    <p><?php echo text_capitalize($actividad_cartelera['actividad_tipo']) ." ". $actividad_cartelera['actividad_formato'] ." ". $actividad_cartelera['actividad_fechas'];?></p>
                    <p class="text-red">Ver más...</p>
                    <p><?php echo text_trim($actividad_prox['actividad_descripcion'],150);?></p>
                </div>
            </a>
            <?php } ?>
        </div>
    </div> -->

    <!-- EVENTOS REALIZDOS conversatorio-->
    <?php $actividades_realizados = getActividadesListProximos(3,'all', "next", "past");
    // print_r();
    ?>

    <!-- <div class="segment_proximos_title layout-box-fat background-gris" style="background-color: black; width:100%">
        <h3>Eventos Realizados</h3>
        <hr>
        <div class="segment_proximos_eventos">
            <?php foreach($actividades_realizados as $actividad_realizada){ ?>
            <a class="card flex-30" href="actividad.php?id=<?php echo $actividad_realizada['actividad_id']?>">
                <div class="card_image" style="background-image:url(files/actividad/<?php echo $actividad_realizada['actividad_id']?>/portada_h.jpg);">
                    <div class="card_image_type"> <?php echo text_capitalize($actividad_realizada['actividad_tipo'])?> </div>
                </div>

                <div class="card_description flex-1">
                    <h3><?php echo $actividad_realizada['actividad_nombre']?></h3>
                    <p class="text-grey text-small"><?php echo $actividad_realizada['actividad_fechas'];?></p>
                    <p class="text-red">Ver más...</p>
                    <!-- <p><?php echo text_trim($actividad_realizada['actividad_descripcion'],150);?></p> -->
                </div>
            </a>
            <?php } ?>
        </div>
    </div> -->

    <!-- NOTICIAS -->
    <?php $noticias = getNoticiasList(3);
    ?>
    <div class="segment_proximos_title layout-box-fat" style="width: 100%; background-color: black;">
        <h3>Convocatorias</h3>
        <hr>
        <div class="segment_proximos_eventos flex-wrap">
        <?php 
            foreach($noticias as $noticia){ ?>
            <a class="card flex-30" href="noticia.php?id=<?php echo $noticia['convocatoria_id']?>">
                <div class="card_image" style="background-image:url(files/convocatoria/<?php echo $noticia['convocatoria_imagen']?>);">
                    <div class="card_image_type"> <?php echo text_capitalize("Convocatoria")?> | <?php echo display_date_MMYYYY($noticia['convocatoria_fecha'])?> </div>
                </div>

                <div class="card_description">
                    <h4><?php echo $noticia['convocatoria_nombre']?></h4>
                </div>
            </a>
            <?php } ?>
        </div>
    </div>

    <!-- PREGUNTA -->
    <div class="segment_pregunta" style="background-color:black; width: 100%;">
        <?php include "inc/component/pregunta.php" ?>
    </div>
    

    <!-- COLABORADORES  -->
    <?php $colaboradores = getColaboradoresList(3, "compania");?>
    <!-- <div class="segment_compañias background-negro" style="    width: 100%;
    padding: 2rem 10% 0 10%;">
        <h3>Red Arística</h3>
        <div class="layout-box background-blanco" style="display:flex;">
            <?php foreach($colaboradores as $colaborador){ ?>
                <div class=" flex-1" style="flex: 1 1 20%;margin-left:2rem;">
                <img src="files/colaboradores/<?php echo $colaborador['realizador_id'] ?>.png" alt="" style="width:100%; ">
                    <h3><?php echo $colaborador['realizador_nombre']?></h3>
                    <p><?php echo text_trim($colaborador['realizador_descripcion'],150);?></p>
                </div>
            <?php } ?>

        </div>
    </div> -->

</div>

<?php include "footer.php";?>

<script>
    $(function() {
        count = 0;
        wordsArray = ["Diversidad", "Cultura", "Participación", "patrimonio", "Comunidad", "Artes", "Teatro", "Autogestión"];

        duration = 1000;
        interval = 500;

        setInterval(function() {
            count++;
            $("#header_variable").fadeOut(interval, function() {
                $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                    interval);
            });
        }, Math.floor(Math.random() * interval + duration));

        img_count = 0;
        imgArray = ["files/imagenes/dianaactividades_0001.webp", "files/imagenes/dianaactividades_0002.webp", "files/imagenes/dianaactividades_0003.webp"];

        img_duration = 3333;
        img_interval = 1500;

        setInterval(function() {
            img_count++;
            $("#header_image").fadeOut(img_interval, function() {
                $(this).attr("src",imgArray[Math.floor(Math.random() * imgArray.length)]).fadeIn(
                    img_interval);
            });
        }, Math.floor(Math.random() * img_interval + img_duration));
    });

    $('#carousel-inner div:first-child').addClass('active');
    $('#carousel_PEF div:first-child').addClass('active');
</script>