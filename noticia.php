<?php   
    include 'inc/template/navbar.php';
    include 'inc/php/actividades_list.php';

    if (isset($_GET['id'])) {
        $id = $_GET['id'];
    }

    $convocatoria = getConvocatoriaInfo($id);
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index_main.php">Inicio</a></li>
    <li class="breadcrumb-item"><a href="noticias.php">Noticias</a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo text_capitalize($convocatoria['convocatoria_nombre'])?></li>
  </ol>
</nav>

<div class="contenedor">

<div class="segment_actividad " style="display: flex; min-height: 50vw;">
    <div class="col background-image" style="background-image:url(files/convocatoria/<?php echo $convocatoria['convocatoria_imagen']?>); display:flex; flex-flow:column; justify-content:center; align-items:center;">
        <div class="card_image_type"> <?php echo text_capitalize("Convocatoria")?> </div>
        <h1><?php echo text_capitalize($convocatoria['convocatoria_nombre'])?></h1>
    </div>
    <div class="col layout-box-small">
        <h3><?php echo text_capitalize($convocatoria['convocatoria_nombre'])?></h3>
        <p><?php echo text_capitalize($convocatoria['convocatoria_descripcion'])?></p>
        <hr>
        <p><span class="text-thin">Categorias: </span><?php echo $convocatoria['convocatoria_categoria']?></p>
        <hr>
        <p><span class="text-thin">Link: </span><a href="<?php echo $convocatoria['convocatoria_link']?>"><?php echo $convocatoria['convocatoria_link']?></a></p>
        <hr>

    </div>
</div>

<!-- Modal markup: https://getbootstrap.com/docs/4.4/components/modal/ -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        
      <!-- Carousel markup: https://getbootstrap.com/docs/4.4/components/carousel/ -->
      <div id="carouselExample" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="/image-1.jpg">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="/image-2.jpg">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="/image-3.jpg">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="/image-4.jpg">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="segment_actividad_comentarios">
    <!-- <div class="segment_actividad_comentarios">
        <p>
            "Una obra que sustituye a las seis mujeres que protagonizan la obra por seis “hombres”, quienes tensionan la idea de género, sexo y rol social a desempeñar."
        </p>
        <h4>
            Pedro Pérez
        </h4>
    </div>

    <hr>

    <div class="segment_actividad_comentarios">
        <p>
            "Una obra que sustituye a las seis mujeres que protagonizan la obra por seis “hombres”, quienes tensionan la idea de género, sexo y rol social a desempeñar."
        </p>
        <h4>
            Pedro Pérez
        </h4>
    </div>
    <hr> -->
</div>
<!-- 
<div class="segment_actividad_otros">

</div> -->

<div class="segment_proximos_title layout-box">
        <h3>Similares</h3>
        <hr>
        <div class="segment_proximos_eventos">
            <?php 
            foreach($actividades as $otraactividad){ ?>
            <a class="card flex-1" href="actividad.php?id=<?php echo $otraactividad['actividad_id']?>">
                <div class="card_image" style="background-image:url(files/actividad/<?php echo $otraactividad['actividad_id']?>/portada.jpg);">
                    <div class="card_image_type"> <?php echo text_capitalize($otraactividad['actividad_tipo'])?> </div>
                </div>

                <div class="card_description">
                    <h4><?php echo $otraactividad['actividad_nombre']?></h4>
                </div>
            </a>
            <?php } ?>
        </div>
    </div>





</div>

<?php include "footer.php";?>

<script>
    $('#carousel-inner div:first-child').addClass('active');
</script>