        </div><!--body__container END-->
        
        <footer class="footer flex-row layout-box" style="justify-content: space-between;">
            <div class="flex-col  flex-1">
                <img src="files/logodiana_web_negro.png" alt="" style="width:200px; margin-bottom:2rem;">
                <div class="flex-row" style="width: 100%; justify-content: space-evenly;margin-bottom:2rem;">
                    <a href="https://www.facebook.com/espaciodiana/"><i class="fab fa-facebook-f fa-1x" style="margin-left:10px;"></i></a>
                    <a href="https://www.instagram.com/espaciodiana/"><i class="fab fa-instagram fa-1x" style="margin-left:10px;"></i></a>
                    <a href="https://twitter.com/espacio_diana"><i class="fab fa-twitter fa-1x" style="margin-left:10px;"></i></a>
                </div>
                <div class="flex-row" style="width: 100%; justify-content: space-evenly;margin-bottom:2rem;">
                    <a href="https://www.juegosdiana.cl"><img src="files/imagenes/logojd.png" alt="" style="width: 100px; height: 100px;; margin-bottom:2rem;"></a>
                    <a href="https://www.ladiana.cl"><img src="files/imagenes/logold.png" alt="" style="width: 100px; height: 100px;; margin-bottom:2rem;"></a>
                    <a href="#"><img src="files/imagenes/gob.png" alt="" style="width: 100px; height: 100px;; margin-bottom:2rem;"></a>
                </div>
            </div>

            <div class="flex-col flex-1">
                <ul class="navbar-nav mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="nosotros.php">Nosotros</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="actividades.php">Calendario</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="noticias.php">Noticias</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="contacto.php">Contacto</a>
                    </li>
                </ul>
            </div>

            <div class="flex-col flex-1">
                <h4>Suscribete para más información</h4>
                <form action="subscribe">
                    <div class="form-group">
                        <label for="exampleInputEmail1" style="font-size:1.6rem;">Email</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        <small id="emailHelp" class="form-text text-muted"  style="font-size:1.6rem;">No compartiremos tu información.</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Suscribirse</button>
                </form>
            </div>
        </footer>

        <!-- TOAST -->
        <div class="toast" style="position: fixed; top: 60px; right: 20px;">
            <div class="toast-header">
            <strong class="mr-auto" id="toast-title">Bootstrap</strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="toast-body" id="toast-message">
            Hello, world! This is a toast message.
            </div>
        </div>

    </body>
</html>

<script src="inc/js/functions.js"></script>

<?php
$actualPage = getPage();

 if ($actualPage === 'login') {
     echo '<script src="inc/js/login.js"></script>';
 }else if($actualPage === 'reserva'){
    echo '<script src="inc/js/listener.js"></script>';
 }
 ?>

<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
    crossorigin="anonymous"></script>
<script src="inc/js/parallax.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
    integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
</script>

<script>
$('.dropdown-toggle').dropdown();
$('.toast').toast({delay:2000})




</script>