<?php 

use \Freshwork\Transbank\CertificationBagFactory;
use \Freshwork\Transbank\TransbankServiceFactory;

include 'vendor/autoload.php';

$bag = CertificationBagFactory::integrationWebpayNormal();
$webpay = TransbankServiceFactory::normal($bag);

$webpay->addTransactionDetail(10000,'123456789');
$response = $webpay->initTransaction('https://localhost/espaciodiana/response.php', 'https://localhost/espaciodiana/finish.php');
print_r($response);

?>

<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Webpay</title>
</head>
<body>
<div class="container">
<h2>Pago con Webpay</h2>
<p>Valor: <?php echo $amount;?></p>
<p>Orden de Compra: <?php echo $buyOrder;?></p>
</div>
    <form action="<?php echo $formAction ?>" method="POST" class="form-inline" role="form">

        <input type="hidden" name="campoToken" value="<?php echo $tokenWs ?>">
        <button type="submit" class="btn btn-primary">Pagar</button>

    </form>
</body>
</html> -->