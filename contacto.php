<?php   
    include 'inc/template/navbar.php';
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index_main.php">Inicio</a></li>
    <li class="breadcrumb-item active" aria-current="page">Contacto</li>
  </ol>
</nav>

<div class="contenedor">
    <div class="segment_cabecera background-image" style="background-image: url('files/imagenes/img_0001_diana002.jpg');">

        <div class="segment_cabecera_contenedor">
            <div class="segment_cabecera_titulo">
                <h1>Contacto</h1>
            </div>
        </div>
    </div>


    <div class="segment_historia flex-row">
        <div class="fondo">
            <div class="flex-3"></div>
            <div class="flex-2 background-rojo"></div>
        </div>

        <div class="flex-col flex-1 layout-box-left historia_prev_content">
            <p>
                Queremos saber de ti, comentanos si tienes alguna duda o quieres dejarnos un mensaje.
            </p>
            <form style="    width: 90%;">
                <div class="form-group">
                    <label for="exampleFormControlInput1" style="font-size:1.4rem">Nombre</label>
                    <input type="texto" class="form-control" id="exampleFormControlInput1" placeholder="nombre">
                </div>
                <div class="form-group" style="font-size:1.4rem">
                    <label for="exampleFormControlInput1">Email</label>
                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="email@ejemplo.com">
                </div>
                <div class="form-group" style="font-size:1.4rem">
                    <label for="exampleFormControlTextarea1">Mensaje</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </form>
        </div>
        
    </div>
</div>

<?php include "footer.php";?>
