<?php   
    include 'inc/template/navbar.php';
    include 'inc/php/actividades_list.php';

?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index_main.php">Inicio</a></li>
    <li class="breadcrumb-item active" aria-current="page">Cartelera</li>
  </ol>
</nav>

<div class="contenedor">

    <div class="segment_cabecera background-image" style="background-image: url('files/imagenes/img_0001_diana002.jpg');">

        <div class="segment_cabecera_contenedor">
            <div class="segment_cabecera_titulo">
                <h1>NUESTRA CARTELERA</h1>
            </div>
        </div>

    </div>

    <!-- <div class="segment_proximos_title layout-box" id="familiar">
        <h3>Familiar</h3>
        <hr>
        <div class="segment_proximos_eventos">
            <?php 
            $actividades = getActividadesListProximos(4, 'obra', 'all','active', 'familiar');
            foreach($actividades as $actividad){ ?>
            <a class="card flex-1" href="actividad.php?id=<?php echo $actividad['actividad_id']?>">
                <div class="card_image" style="background-image:url(files/actividad/<?php echo $actividad['actividad_id']?>/portada_h.jpg);">
                    <div class="card_image_type"> <?php echo text_capitalize($actividad['actividad_tipo'])?> | Familiar </div>
                </div>

                <div class="card_description flex-1">
                    <h3><?php echo $actividad['actividad_nombre']?></h3>
                    <p><?php echo $actividad['actividad_fechas'];?></p>
                    <hr>
                    <p><?php echo text_trim($actividad['actividad_descripcion'],250);?></p>
                    <p class="text-red">Ver más</p>
                </div>
            </a>
            <?php } ?>
        </div>
    </div> -->

    <div class="segment_proximos_title layout-box" id="teatro">
        <h3>Teatro</h3>
        <hr>
        <div class="segment_proximos_eventos">
            <?php 
            $actividades = getActividadesListProximos(4, 'obra', 'all','active');
            foreach($actividades as $actividad){ ?>
            <a class="card flex-1" href="actividad.php?id=<?php echo $actividad['actividad_id']?>">
                <div class="card_image" style="background-image:url(files/actividad/<?php echo $actividad['actividad_id']?>/portada_h.jpg);">
                    <div class="card_image_type"> <?php echo text_capitalize($actividad['actividad_tipo'])?> | <?php echo $actividad['actividad_publico']?></div>
                </div>

                <div class="card_description flex-1">
                    <h3><?php echo $actividad['actividad_nombre']?></h3>
                    <p><?php echo $actividad['actividad_fechas'];?></p>
                    <hr>
                    <p><?php echo text_trim($actividad['actividad_descripcion'],150);?></p>
                    <p class="text-red">Ver más</p>
                    
                </div>
            </a>
            <?php } ?>
        </div>
    </div>

<!-- TALLERES -->

    <div id="taller" class="segment_proximos_title layout-box">
        <h3>Talleres</h3>
        <hr>
        <div class="segment_proximos_eventos">
            <?php 
            $actividades = getActividadesListProximos(4, 'taller', 'all','active');
            foreach($actividades as $actividad){ ?>
            <a class="card flex-1" href="actividad.php?id=<?php echo $actividad['actividad_id']?>">
                <div class="card_image" style="background-image:url(files/actividad/<?php echo $actividad['actividad_id']?>/portada.jpg);">
                    <div class="card_image_type"> <?php echo text_capitalize($actividad['actividad_tipo'])?></div>
                </div>

                <div class="card_description flex-1">
                    <h4><?php echo $actividad['actividad_nombre']?></h4>
                    <p><?php echo $actividad['actividad_fechas'];?></p>
                    <hr>
                    <p class="text-red">Ver más</p>
                    
                </div>
            </a>
            <?php } ?>
        </div>
    </div>

    <!--<div class="segment_proximos_title layout-box">
        <h3>Conciertos</h3>
        <hr>
        <div class="segment_proximos_eventos">
            <?php $actividades = getActividadesListProximos(4, 'concierto');
            foreach($actividades as $actividad){ ?>
            <a class="card flex-1" href="actividad.php?id=<?php echo $actividad['actividad_id']?>">
                <div class="card_image" style="background-image:url(files/actividad/<?php echo $actividad['actividad_id']?>/portada.jpg);">
                    <div class="card_image_type"> <?php echo text_capitalize($actividad['actividad_tipo'])?> </div>
                </div>

                <div class="card_description flex-1">
                    <h3><?php echo $actividad['actividad_nombre']?></h3>
                    <p><?php echo displayFechasActividad($actividad);?></p>
                    <p class="text-red">Comprar entradas</p>
                    <p><?php echo text_trim($actividad['actividad_descripcion'],150);?></p>
                </div>
            </a>
            <?php } ?>
        </div>
    </div> -->

    <div class="segment_proximos_title layout-box">
        <h3>Conversatorios</h3>
        <hr>
        <div class="segment_proximos_eventos">
            <?php $actividades = getActividadesListProximos(4, 'conversatorio', 'all','active');
            foreach($actividades as $actividad){ ?>
            <a class="card flex-1" href="actividad.php?id=<?php echo $actividad['actividad_id']?>">
                <div class="card_image" style="background-image:url(files/actividad/<?php echo $actividad['actividad_id']?>/portada_h.jpg);">
                    <div class="card_image_type"> <?php echo text_capitalize($actividad['actividad_tipo'])?></div>
                </div>

                <div class="card_description flex-1">
                    <h3><?php echo $actividad['actividad_nombre']?></h3>
                    <p><?php echo $actividad['actividad_fechas'];?></p>
                    <hr>
                    <p><?php echo text_trim($actividad['actividad_descripcion'],150);?></p>
                    <p class="text-red">Ver más</p>
                </div>
            </a>
            <?php } ?>
        </div>
    </div>

</div>

<?php include "footer.php";?>

<script>
    $('#carousel-inner div:first-child').addClass('active');
</script>