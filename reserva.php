<?php   
    include 'inc/template/navbar.php';
    include 'inc/php/actividades_list.php';

    if (isset($_GET['id'])) {
        $id = $_GET['id'];
    }

    $funcion = getFuncionInfo($id);
?>

<style>
    body{
        font-size:16px;
    }
</style>

<div class="contenedor">
    <div class="segment_cabecera background-image" style="background-image: url('files/imagenes/img_0001_diana002.jpg');">

        <div class="segment_cabecera_contenedor">
            <div class="segment_cabecera_titulo">
                <h1>Boletería</h1>
            </div>
        </div>
    </div>
</div>

<div class="segment_boleteria flex-col layout-box">
<h2><?php echo $funcion['actividad_nombre'];?></h2>

<hr>
<div class="flex-row">
    <div class="container flex-1">
        <div class="fecha flex-col">
            <div class="flex-row">
                <div class="fecha_fecha">
                    <p><?php echo displayFecha($funcion['funcion_dia']);?> / <?php echo displayHora($funcion['funcion_hora']);?></p>
                </div>
            </div>
        </div>
        
        <p>a la gorra</p>
    </div>

    <form id="realizarReserva_form" class="form-inline flex-1 flex-col; " role="form">

        <!-- INFORMACION - NOMBRE DEL ASISTENTE -->
        <div class="form-group" style="flex-flow: column; align-items: baseline; margin-bottom:10px; width: 60%;">
            <label for="exampleInputEmail1">Nombre</label>
            <input name="nombre0" type="text" class="form-control" id="nombre0" aria-describedby="emailHelp" style="width:100%">
        </div>

        <!-- INFORMACION - EMAIL DEL ASISTENTE -->
        <div class="form-group" style="flex-flow: column; align-items: baseline; margin-bottom:10px; width: 60%;">
            <label for="exampleInputEmail1">Email</label>
            <input name="email" type="email" class="form-control" id="email" aria-describedby="emailHelp" style="width:100%">
            <small class="form-text text-muted">No compartiremos tu información</small>
        </div>


        <!-- INFORMACION PARA EL TALLER -->
        <?php if($funcion['actividad_tipo']=="taller"){?>

            <div class="form-group" style="flex-flow: column; align-items:baseline; margin-bottom:10px; width: 60%;">
                <label for="nombre_nino">Nombre del niño</label>
                <input name="nombre_nino" type="text" class="form-control" id="nombre_nino" aria-describedby="emailHelp" style="width:100%">

                <label for="telefono">Teléfono</label>
                <input name="telefono" type="text" class="form-control" id="telefono" aria-describedby="emailHelp" style="width:100%" placeholder="+569 xxxx xxxx">
            </div>

        <?php
        }
        ?>
        <!--  -->


        <input type="hidden" id="funcion_id" value="<?php echo $id ?>">
        <input type="hidden" name="valor" value="<?php echo $funcion['funcion_valor'];?>">
        <input type="hidden" name="tipoEntrada" value="<?php echo $funcion['funcion_pago'];?>">
        <button type="submit" class="btn btn-red btn-pago" style="width: 60%;">Reservar</button>
    </form>
</div>

</div>


<?php include "footer.php";?>

<script>
    $('#carousel-inner div:first-child').addClass('active');

    $("#entradas").change(function () {
        var numInputs = $(this).val();
        if(numInputs == 1){
            $("#inputArea").children().remove();
        }else{
                $("#inputArea").children().remove();
                $("#inputArea").append('<label for="exampleInputEmail1">Nombre para las entradas adicionales:</label>');
            for (var i = 1; i < numInputs; i++){
                
                $("#inputArea").append('<input id="nombre'+i+'" type="text" class="form-control" aria-describedby="emailHelp" style="width:100%">');
            }
        }
    }
);
</script>