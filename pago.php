<?php   
    include 'inc/template/navbar.php';
    include 'inc/php/actividades_list.php';

    if (isset($_GET['id'])) {
        $id = $_GET['id'];
    }

    $funcion = getFuncionInfo($id);
    // print_r($funcion);

    if($funcion['funcion_pago']=="entrada"){ 
        $redirect="pago_confirmacion.php";
        $redirect_text ="Pagar";
    }else if($funcion['funcion_pago']=="gorraon"){
        $redirect="pago_confirmacion.php";
        $redirect_text ="Pagar";
    }else if($funcion['funcion_pago']=="gorra"){
        $redirect="reserva_confirmacion.php";
        $redirect_text ="Reservar";
    }else if($funcion['funcion_pago']=="reserva"){
        $redirect="reserva_confirmacion.php";
        $redirect_text ="Reservar";
    }

    $inicio_obra = strtotime($funcion['funcion_hora']);

    $duracion = $funcion['funcion_duracion'] * 3600;
    $fin_obra = $inicio_obra + $duracion;

    $h = ($fin_obra / 3600 % 24)+1;
    $m = $fin_obra / 60 % 60; 
?>

<style>
    body{
        font-size:16px;
    }
</style>
<div class="contenedor">
    <div class="segment_cabecera background-image" style="background-image: url('files/imagenes/img_0001_diana002.jpg');">

        <div class="segment_cabecera_contenedor">
            <div class="segment_cabecera_titulo">
                <h1>Boletería</h1>
            </div>
        </div>
    </div>
</div>

<div class="segment_boleteria flex-col layout-box">
<h2><?php echo $funcion['actividad_nombre'];?></h2>

<hr>
<div class="flex-row">
    <div class="container flex-1">
        <div class="fecha flex-col">
            <div class="flex-row">
                <div class="fecha_fecha">
                    <p><?php echo displayFecha($funcion['funcion_dia']);?> / <?php echo displayHora($funcion['funcion_hora']);?></p>
                </div>
            </div>
        </div>
        
        <?php if($funcion['funcion_pago']=="entrada"){ ?>
            <p>Valor: $<?php echo $funcion['funcion_valor'];?> CLP</p>

        <?php }else if($funcion['funcion_pago']=="gorraon"){ ?>
            <p>Función a la gorra. </p>
            <p>Puede realizar un aporte desde $1.000.</p>
            <p>Al momento del pago se genera un mail con una clave de acceso que llegara al correo ingresado y podrá ser usada una sola vez (revisar carpeta spam).</p>

            <?php 
            if ($funcion['actividad_tipo'] == "obra") {
            ?>
            
            <p>Podrás disfrutar de la función, ingresando a la plataforma el día de la función desde las <?php echo $funcion['funcion_habilitado'];?> hasta las <?php echo $funcion['funcion_fin'];?> horas. </p>
            
            <?php 
                }else if($funcion['actividad_tipo'] == "taller"){
            ?>

            <p>Se te enviara un código de acceso con la información de la función, ingresando a la plataforma el día de la función a las <?php echo $funcion['funcion_hora'];?> horas. </p>
            
            <?php
                }
            ?>

        <?php } ?>
    </div>

    <form action="<?php echo $redirect?>" method="POST" class="form-inline flex-1 flex-col; " role="form">

        <div class="form-group" style="flex-flow: column; align-items: baseline; margin-bottom:10px; width: 60%;">
            <label for="exampleInputEmail1">Email</label>
            <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" style="width:100%">
            <small id="emailHelp" class="form-text text-muted">No compartiremos tu información</small>
        </div>

        <?php 
        if($funcion['funcion_pago']=="entrada"){
            ?>

        <!-- VENTA ENTRADA NORMAL -->
        <div class="form-group" style="flex-flow: column; align-items:baseline; margin-bottom:10px; width: 60%;">
            <label for="exampleFormControlSelect1">Cantidad de Entradas</label>
            <select class="form-control" id="entradaspago" name="entradas"> 
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
        </div>

        <input type="hidden" name="valor" value="<?php echo $funcion['funcion_valor'];?>">

        <?php }else if($funcion['funcion_pago']=="gorraon"){?>

        <!-- VENTA A LA GORRA ONLINE -->
        <div class="form-group" style="flex-flow: column; align-items:baseline; margin-bottom:10px; width: 60%;">
            <label for="exampleFormControlSelect1">Realiza un aporte</label>
            <small class="form-text text-muted">(aporte mínimo $1.000 pesos)</small>
            <div style="display: flex;"><select class="form-control" id="entradasgorraonline" name="valor"> 
                <option value="1000">$1.000</option>
                <option value="2000">$2.000</option>
                <option value="5000">$5.000</option>
                <option value="10000">$10.000</option>
                <option value="otro">otro</option>
            </select>
            <input name="entradasgorraonline_input" disabled type="number" class="form-control" id="entradasgorraonline_input" placeholder="otro valor" style="margin-left:5px; width: 100%;"></div>
        </div>

        <?php
        }
        ?>

        <div class="form-group" style="flex-flow: column; align-items: baseline; margin-bottom:10px; width: 60%;">
            <label for="exampleInputEmail1">Nombre</label>
            <input name="nombre0" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" style="width:100%">

            <div id="inputArea">
            </div>

            <small id="emailHelp" class="form-text text-muted">No compartiremos tu información</small>
        </div>

        <!-- INFORMACION PARA EL TALLER -->
        <?php if($funcion['actividad_tipo']=="taller"){?>

        <div class="form-group" style="flex-flow: column; align-items:baseline; margin-bottom:10px; width: 60%;">
            <label for="nombre_nino">Nombre del niño</label>
            <input name="nombre_nino" type="text" class="form-control" id="nombre_nino" aria-describedby="emailHelp" style="width:100%">

            <label for="telefono">Teléfono</label>
            <input name="telefono" type="text" class="form-control" id="telefono" aria-describedby="emailHelp" style="width:100%" placeholder="+569 xxxx xxxx">
        </div>

        <?php
        }
        ?>
        <!--  -->

        <input type="hidden" name="funcion" value="<?php echo $id ?>">
        <input type="hidden" name="tipoEntrada" value="<?php echo $funcion['funcion_pago'];?>">
        <button type="submit" class="btn btn-red btn-pago" style="width: 60%;"><?php echo $redirect_text ?></button>
    </form>
</div>

</div>


<?php include "footer.php";?>

<script>
    $('#carousel-inner div:first-child').addClass('active');

    $("#entradaspago").change(function () {
        var numInputs = $(this).val();
        if(numInputs == 1){
            $("#inputArea").children().remove();
        }else{
                $("#inputArea").children().remove();
                $("#inputArea").append('<label for="exampleInputEmail1">Nombre para las entradas adicionales:</label>');
            for (var i = 1; i < numInputs; i++){
                
                $("#inputArea").append('<input name="nombre'+i+'" type="text" class="form-control" aria-describedby="emailHelp" style="width:100%">');
            }
        }
    });

    $("#entradasgorraonline").change(function () {
        var value = $(this).val();
        console.log(value);
        if(value == "otro"){
            $("#entradasgorraonline_input").prop("disabled", false);
            $("#entradasgorraonline_input").focus();
        }else{
            $("#entradasgorraonline_input").prop("disabled", true);
        }
    });
</script>