<script type="text/javascript">
document.cookie = 'window_width=' + window.innerWidth;
</script>

<?php
  require 'inc/template/navbar_landing.php';
  require 'inc/php/pregunta_list.php';

  $preguntas = get_preguntas_list();
  $preguntas_fondo = get_preguntas_list_parafondo();
  $cantidad = get_preguntas_amount();

  $width = $_COOKIE['window_width'];

  $calls = 11;

  if($width <= '1000'){
    $calls = 8;
  }
?>

<html>

<head>
    <link href="inc/css/calendar.css" type="text/css" rel="stylesheet" />
</head>

<body>
    <div class="contenedor">
        <!-- <div class="segment_header">

          <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active" style="background-image:url(files/imagenes/espaciodiana_teatro1.jpeg);">
                <div class="carousel-container">
                  <h1>Nuevas cosas están ocurriendo en <span class="rojo">Diana</span>...</h1>
                </div>
              </div>

              <div class="carousel-item" style="background-image:url(files/imagenes/espaciodiana_teatro2.jpeg);">
                <div class="carousel-container">
                  <h1>Nuevas cosas están ocurriendo en <span class="rojo">Diana</span>...</h1>
                </div>
              </div>

              <div class="carousel-item" style="background-image:url(files/imagenes/espaciodiana_teatro3.jpeg);">
                  <div class="carousel-container">
                <h1>Nuevas cosas están ocurriendo en <span class="rojo">Diana</span>...</h1>
              </div>
              </div>
            </div>
          </div>
      </div> -->



        <div class="segment_pregunta">
            <div class="graffiti_fondo">
                <?php

            for( $i = 0; $i <= $calls; $i++){
              $margintop = ($i * rand ( 5 , 15 ) )-20;
              $marginleft = ($i * rand ( 0 , 5 ))-20;?>
                <h3 class="item" id="word<?php echo $i; ?>" style="margin-left:<?php echo $marginleft;?>px; margin-top:<?php echo $margintop;?>px;"><?php echo $preguntas_fondo[$i];?></h3>

                <?php }?>
            </div>

            <div class="pregunta_container" style="">
                <img src="files/logodiana_isotipo_negro.png" alt="" style="width:30%;">

                <form id="pregunta_form" action="pregunta" style="width: 100%;">
                  <h3 style="text-align: center;">¿Qué te gustaría que pasara aquí?</h3>
                  <?php 
                  $placeholder_options = array('Quiero un unicornio de muchos colores' , 'Quiero tocar en una banda de rock', 'Quiero ser el mejor gamer de todos los tiempos');
                  $placeholder = $placeholder_options[rand(0,2)];?>
                    <div class="input-group">
                        <textarea id="pregunta_respuesta" class="form-control" aria-label="With textarea"
                            placeholder="<?php echo $placeholder;?>" data-container="body"
                            data-toggle="popover" data-placement="bottom"
                            data-content="¿Seguro que no te interesa que pasen cosas en este lugar?"></textarea>
                    </div>

                    <br>

                    <div class="form-group">
                        <input type="text" class="form-control" id="pregunta_usuario" aria-describedby="emailHelp"
                            placeholder="Hola, me llamo..." data-container="body" data-toggle="popover"
                            data-placement="bottom" data-content="Mi nombre es Diana, ¿Y el tuyo?.">
                        <small class="form-text text-muted">No compartiremos tu información.</small>
                    </div>

                    <button type="submit" class="btn btn-primary btn-lg">Enviar</button>
                </form>
                <div class="segment_resumen">
                  <p>Espacio Diana un lugar de encuentro cultural - artes / gastronomía / entretención</p>
                  <p>Arturo Prat N°435  / contacto@juegosdiana.cl / +56 9 6683 5598</p>
                </div>

            </div>
        </div>

        <!-- <div class="segment_resumen">
            <div class="segment_resumen_descripcion"
                style="background-image:url(files/imagenes/espaciodiana_capa3.png);">
                <div class="segment_col">
                    <h2>Espacio Diana</h2>
                    <h4>
                        Es un centro de Artes , gastronomia y entretención, situado en el corazón del Barrio San Diego,
                        en un edificio patrimonial de principios del siglo XX y emplazado detrás del tradicional parque
                        de entretenciones Juegos Diana.
                    </h4>
                </div>

                <div class="segment_col">
                    <p><i class="fas fa-phone"></i> +56 2 26711244 / +569 6683 5598 </p>
                    <p><i class="fas fa-envelope-open-text"></i> contacto@juegosdiana.cl</p>
                    <p><i class="fas fa-building"></i> Arturo Prat N°435</p>
                </div>
            </div>
        </div> -->

        <div class="segment_cierre">
        
        </div>
    </div>
</body>

</html>


<div id="modalNewsletter" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Gracias <span id="nombre_usuario"></span> </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3>Estamos buscando dar vida a este espacio, con todas nuestras ideas.</h3>
                <p>Si quieres que te informemos de nuestras novedades, puedes inscribirte en nuestro newsletter.</p>
                <form id="updateEmail_form">
                    <div class="form-group">
                        <input type="text" id="id_usuario" hidden>
                        <input type="email" class="form-control" id="email_usuario" aria-describedby="emailHelp"
                            placeholder="Ingresa tu email">
                        <small id="emailHelp" class="form-text text-muted">No compartiremos tu información.</small>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Quiero estar al tanto</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="modalSuccess" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h3>Tu email ha sido guardado</h3>
                <p>Pronto recibirás más noticias de lo que está sucediendo en Diana.</p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
    crossorigin="anonymous"></script>
<script src="inc/js/parallax.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
    integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
</script>

<script src="inc/js/listener.js"></script>

<script>
$('.carousel').carousel({
    interval: 6000
})


$(function() {
    count = 0;
    wordsArray = <?php echo json_encode($preguntas_fondo);?>;

    duration = 6000;
    interval = 2000;

    setInterval(function() {
        count++;
        $("#word0").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word1").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word2").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word3").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word4").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word5").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word6").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word7").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word8").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word9").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word10").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word11").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));
});
</script>