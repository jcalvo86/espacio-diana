<?php   
    include 'inc/template/navbar.php';
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index_main.php">Inicio</a></li>
    <li class="breadcrumb-item active" aria-current="page">Nosotros</li>
  </ol>
</nav>

<div class="contenedor">
    <div class="segment_cabecera background-image" style="background-image: url('files/imagenes/img_0001_diana002.jpg');">

        <div class="segment_cabecera_contenedor">
            <div class="segment_cabecera_titulo">
                <h1>ESPACIO DIANA, </h1>
            </div>
        </div>
    </div>


    <div class="segment_historia flex-row">
        <div class="fondo">
            <div class="flex-3"></div>
            <div class="flex-2 background-rojo"></div>
        </div>

        <div class="flex-col flex-1 layout-box-left historia_prev_content">
            <h2>Espacio Diana</h2>
            <p>
            Somos un centro cultural independiente y autogestionado, donde converge lo emergente, lo familiar, la búsqueda de nuevos lenguajes, la ciudadanía, el patrimonio y el territorio.
            </p>

            <p>
            Hoy ofrecemos talleres, encuentros, seminarios, temporadas, eventos y coproducciones que apuntan a consolidar una programación artística de compañías independientes. 
                        </p>

            <p>
            Es así, que nuestra programación fomenta actividades, proyectos y procesos culturales con miradas de inclusión, propiciando las temáticas de genero y disidencia sexual, pueblos originarios, medio ambiente y migración, siendo esta última especialmente relevante, ya que el territorio en que nuestro centro cultural  realiza sus actividades, posee una población extranjera importante.            </p>
                    
            <p>
            Nuestro deber como agente cultural es impulsar el derecho al acceso a la cultura, mediante una programación cultural que aporte el aprendizaje, encuentro, reflexión, diálogo y reconocimiento de la diversidad , acorde a la realidad actual chilena, siendo nuestro espacio cultural un aporte para favorecer una sociedad libre de prejuicios y estereotipos culturales.            </p>
        </div>
        
        <div class="flex-col layout-box-right flex-1">
            <div class="block-image-big background-image" style="background-image:linear-gradient(rgba(0,0,0,0),rgba(0,0,0,1)),url(files/imagenes/img_0002_diana003.jpg)">

            </div>
            <div class="block-image-big background-image" style="background-image:linear-gradient(rgba(0,0,0,0),rgba(0,0,0,1)),url(files/imagenes/img_0003_diana004.jpg)">

            </div>
        </div>
    </div>

    <div class="segmento_nosotros flex-row">
        <div class="flex-col flex-1 layout-box-left">
            <h2>Historia</h2>
            <p>Desde hace 15 años, Espacio Diana, es un lugar donde convergen cultura, educación, arte, ocio y entretención. Sus dependencias situadas en el Barrio San Diego, en el corazón de la comuna de Santiago, dan cuenta de un inmueble patrimonial que data de principios del siglo XX, emplazado en los alrededores de la Iglesia de Los Sacramentinos, detrás del tradicional Parque Almagro.</p>
            <p>Los orígenes están asociados a los míticos <a href="https://www.juegosdiana.cl">Juegos Diana</a>. Histórico  parque de entretenciones techado, cuya presencia data en la ciudad desde el año 1934 y ocupa un lugar destacado en el imaginario de varias generaciones de santiaguinos. Desde el año 2016 es administrado por una Fundación Diana Institución de Derecho Privado sin Fines de Lucro.</p>
            <p>Hoy en día nuestras dependencias, cobijan al parque de entretenciones,  al Centro Cultural y al  <a href="https://www.ladiana.cl">Restaurante La Diana</a> que ofrece una gran carta gastronómica nacional e internacional con toques criollos.</p>
        </div>
        <div class="flex-col flex-1 background-image" style="background-image: url('files/imagenes/img_0001_diana002.jpg');width: 100%;">
            
        </div>
    </div>

    <div class="segmento_nosotros flex-row-reverse">
        <div class="flex-col flex-1 layout-box-right">
            <h2>Misión</h2>
            <p>
            Facilitar el desarrollo integral de todas las personas, a través de las promoción, práctica, difusión y fomento de iniciativas culturales de carácter artístico, formativo, educacional, social, científico y tecnológico.      
             </p>
             <p>
             Generar espacios de encuentro entre artistas, gestores, organizaciones culturales y la sociedad civil, fomentando la participación y asociatividad de la ciudadanía, mediante el desarrollo de campañas, ciclos y programaciones culturales, prácticas culturales
             </p>
        </div>
        <div class="flex-col flex-1 background-image" style="background-image: url('files/imagenes/img_0002_diana003.jpg');width: 100%;">
            
        </div>
    </div>

    <div class="segmento_nosotros flex-row">
        <div class="flex-col flex-1 layout-box-left">
            <h2>Visión</h2>
            <p>
            En los próximos años apuntamos a convertirnos en el centro cultural de referencia para los creadores artísticos, compañías y grupos de artistas independientes. Nuestro entusiasmo es fortalecer los procesos creativos, formativos y desarrollo de residencias de creadores nacionales y extranjeros, conectando el trabajo de éstos con redes culturales, públicas, privadas, festivales, entre otros organismos de carácter nacional e internacional.
            </p>
        </div>
        <div class="flex-col flex-1 background-image" style="background-image: url('files/imagenes/img_0003_diana004.jpg');width: 100%;">
            
        </div>
    </div>

    <!-- EQUIPO DIANA -->

    <div class="segmento_nosotros flex-row-reverse" style="width: 100%;">
        <div class="flex-col flex-1 layout-box-right">
            <h2>Equipo Centro Cultural</h2>
            <ul>
                <li> Director y Productor Ejecutivo: Juan Pablo Sanguinetti </li>
                <li> Productor General y Programador: Juan Pablo Martínez </li>
                <li> Jefe Técnico: René Méndez </li>
                <li> Producción y Realización Audiovisual: Julián Rudy </li>
                <li> Marketing y Ventas: Isidora Otero </li>
                <li> Prensa y RRSS: Pía Vergara </li>
                <li> Diseño Gráfico: Rodrigo Hurtado </li>
                <li> Programador WEB: Javier Calvo </li>
            </ul>
        </div>
        <div class="flex-col flex-1 background-image" style="background-image: url('files/imagenes/img_0004_diana005.jpg');width: 100%;">
            
        </div>
    </div>

    <!-- FUNDACION DIANA -->

    <div class="segmento_nosotros flex-row-reverse" style="width: 100%;">
        <div class="flex-col flex-1 layout-box-right">
            <h2>Equipo Fundación Diana:</h2>
            <ul>
                <li> Presidente: Enrique Zuñiga Méndez </li>
                <li> Secretario: Tatiana Emden Chang </li>
                <li> Tesorero: Francisco Miguel Márquez Mayordomo </li>
                <li> Director: Alejandro Alberto Chávez Chávez </li>
                <li> Director: Loreto del Pilar Aravena Soto </li>
            </ul>
        </div>
        <!--<div class="flex-col flex-1 background-image" style="background-image: url('files/imagenes/img_0004_diana005.jpg');width: 100%;">-->
            
        <!--</div>-->
    </div>
</div>

<?php include "footer.php";?>
