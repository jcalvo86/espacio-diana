<?php   
    include 'inc/template/navbar.php';
    include 'inc/php/actividades_list.php';


    if (isset($_GET['id'])) {
        $id = $_GET['id'];
    }

    $actividad = getActividadInfo($id);
    // print_r($actividad);
    $fechas = getActividadFechas($id);
    // print_r($fechas);
    $actividades = getActividadesListProximos(3,$actividad['actividad_tipo']);
    $funcion = $fechas->fetch_assoc();
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index_main.php">Inicio</a></li>
    <li class="breadcrumb-item"><a href="cartelera.php">Cartelera</a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo text_capitalize($actividad['actividad_nombre'])?></li>
  </ol>
</nav>

<div class="contenedor" style="">

  <div class="segment_actividad " style="display: flex; min-height: 50vw;">
    <div class="col background-image" style="background-image:url(files/actividad/<?php echo $actividad['actividad_id']?>/portada.jpg); display:flex; flex-flow:column; justify-content:center; align-items:center;">
        <div class="card_image_type"> <?php echo text_capitalize($actividad['actividad_tipo'])?> </div>
    </div>

    <div class="col layout-box-small">
      <h3><?php echo text_capitalize($actividad['actividad_nombre'])?></h3>
      <h4><?php echo $actividad['realizador_nombre']?></h4>
      <hr>
      <h4>Público: <?php echo text_capitalize($actividad['actividad_publico'])?></h4>
      <h4><?php echo text_capitalize($actividad['actividad_fechas'])?></h4>
      <h4>Formato: <?php echo text_capitalize($actividad['actividad_formato'])?></h4>
      <hr>

      <p><?php echo $actividad['actividad_descripcion']?></p>
      <hr>
      <?php if($actividad['actividad_tipo'] == "obra"){
        $ficha = getObraFichaInfo($id);?>
        <p><span class="text-thin">Compañia: </span><?php echo text_capitalize($actividad['realizador_nombre'])?></p>
          <?php echo displayIfExist("Director: ", $ficha['obraficha_director'])?>
          <?php echo displayIfExist("Elenco: ", $ficha['obraficha_elenco'])?>
          <?php echo displayIfExist("Producción: ", $ficha['obraficha_produccion'])?>
          <?php echo displayIfExist("Dramaturgia: ", $ficha['obraficha_dramaturgia'])?>
          <?php echo displayIfExist("Diseño: ", $ficha['obraficha_diseño'])?>
          <?php echo displayIfExist("Música: ", $ficha['obraficha_musica'])?>
          <?php echo displayIfExist("Asistente de Dirección: ", $ficha['obraficha_asistenteDireccion'])?>
          <?php echo displayIfExist("Astencia Teórica: ", $ficha['obraficha_asistenciaTeorica'])?>
      <?php }else if($actividad['actividad_tipo']=="conversatorio"){
          echo '<iframe src="https://www.facebook.com/plugins/video.php?height=314&href=https%3A%2F%2Fwww.facebook.com%2Fespaciodiana%2Fvideos%2F3733245676708343%2F&show_text=false&width=560" width="560" height="314" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe>";';
      } ?>
      
      <br>
        <?php 
            if ($actividad['actividad_pago'] == "reserva") {
        ?>
                <p>Al momento de la reserva se enviará un mail con una clave de acceso que llegara al correo ingresado y podrá ser usada una sola vez (revisar carpeta spam).</p>

        <?php }else{ ?>
                <p>Al momento del pago se genera un mail con un nombre de usuario y una clave de acceso que llegara al correo ingresado y podrá ser usada una sola vez (revisar carpeta spam).</p>

        <?php } ?>


        <?php 
            if ($actividad['actividad_tipo'] == "obra") {
        ?>
        
        <p>Podrás disfrutar de la función, ingresando a la plataforma el día de la función desde las <?php echo $funcion['funcion_habilitado'];?> hasta las <?php echo $funcion['funcion_fin'];?> horas. </p>
        
        <?php 
            }else if($actividad['actividad_tipo'] == "taller"){
        ?>

        <p>Se te enviara un email confirmando la asistencia y entregando los datos para poder ingresar al taller.</p>
        

        <?php
            }
        ?>

    </div>
  </div>
  
  <div class="segment_fechas layout-box">
    <?php foreach($fechas as $fecha){
        
        ?>
    <div class="fecha flex-col">

        <div class="flex-col">
            <div class="fecha_fecha">
                <p><?php echo displayFecha($fecha['funcion_dia']);?> / <?php echo displayHora($fecha['funcion_hora']);?></p>
            </div>
            <div class="fecha_dia">
                <p></p>
            </div>
        </div>

        <?php 
        if($fecha['funcion_pago']=="gorra"){?>
            <div class="fecha_disponibles">
                <p> a la gorra</p>
            </div>

            <div class="fecha_boton">
            <?php if($fecha['funcion_entradas_disponibles'] <=0){
                ?>
                <div class="btn btn-red btn-disabled">
                    <p> Entradas Agotadas</p>
                </div>
                
                <?php
            } else{
                ?>
                <a class="btn btn-red" href="reserva.php?id=<?php echo $fecha['funcion_id']?>">
                    <p> Entrada</p>
                </a>
                
                <?php
            }?>



            </div>
        <?php }else if($fecha['funcion_pago']=="reserva"){?>

            <div class="fecha_disponibles">
                <!-- <p><?php echo displayCantidadEntradas($fecha['funcion_entradas_disponibles']);?></p> -->
            </div>

            <div class="fecha_boton">

                <a class="btn btn-red "  href="reserva.php?id=<?php echo $fecha['funcion_id']?>">
                <p> Entrada</p>
                </a>
            </div>

        <?php }else{?>

            <div class="fecha_disponibles">
                <!-- <p><?php echo displayCantidadEntradas($fecha['funcion_entradas_disponibles']);?></p> -->
            </div>

            <div class="fecha_boton">

                <a class="btn btn-red "  href="pago.php?id=<?php echo $fecha['funcion_id']?>">
                <p> Entrada</p>
                </a>
            </div>
        <?php }?>
        
    </div>
    <hr>
    <?php }?>
  </div>
</div>


<div class="segment_actividad_comentarios">
    <!-- <div class="segment_actividad_comentarios">
        <p>
            "Una obra que sustituye a las seis mujeres que protagonizan la obra por seis “hombres”, quienes tensionan la idea de género, sexo y rol social a desempeñar."
        </p>
        <h4>
            Pedro Pérez
        </h4>
    </div>

    <hr>

    <div class="segment_actividad_comentarios">
        <p>
            "Una obra que sustituye a las seis mujeres que protagonizan la obra por seis “hombres”, quienes tensionan la idea de género, sexo y rol social a desempeñar."
        </p>
        <h4>
            Pedro Pérez
        </h4>
    </div>
    <hr> -->
</div>
<!-- 
<div class="segment_actividad_otros">

</div> -->

<div class="segment_proximos_title layout-box">
        <h3>Similares</h3>
        <hr>
        <div class="segment_proximos_eventos">
            <?php 
            foreach($actividades as $otraactividad){ ?>
            <a class="card flex-1" href="actividad.php?id=<?php echo $otraactividad['actividad_id']?>">
                <div class="card_image" style="background-image:url(files/actividad/<?php echo $otraactividad['actividad_id']?>/portada.jpg);">
                    <div class="card_image_type"> <?php echo text_capitalize($otraactividad['actividad_tipo'])?> </div>
                </div>

                <div class="card_description">
                    <h4><?php echo $otraactividad['actividad_nombre']?></h4>
                </div>
            </a>
            <?php } ?>
        </div>
    </div>





</div>

<?php include "footer.php";?>

<script>
    $('#carousel-inner div:first-child').addClass('active');
</script>