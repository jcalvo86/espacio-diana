<?php   
    include '../inc/template/escritorio_navbar.php';
    include '../inc/php/actividades_list.php';

    $obras = getActividadesListAll();

    $entradas = getEntradasListAll();
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="../index_main.php">Inicio</a></li>
    <li class="breadcrumb-item"><a href="index.php">Escritorio</a></li>
    <li class="breadcrumb-item active" aria-current="page"><a>Funciones</a></li>
  </ol>
</nav>
<div class="card__container--wrap">
    <?php foreach($obras as $obra){
        ?>
        <a class="card_horizontal flex-1" href="actividad.php?id=<?php echo $obra['actividad_id']?>">
            <div class="card_image" style="background-image:url(../files/actividad/<?php echo $obra['actividad_id']?>/portada_h.jpg);">
            </div>

            <div class="card_description flex-1">
                <h4><?php echo $obra['actividad_nombre']?></h4>
                <p><?php echo $obra['actividad_fechas'];?></p>
                <p><?php echo $obra['actividad_entradas_aceptadas'];?> Entradas adquiridas, $<?php echo $obra['actividad_monto_recolectado'];?> recolectado</p>
                <hr>

                <?php 
                    $funciones = getFuncionesListForActividad($obra['actividad_id']);
                    $entrada_cantidad = 0;
                    foreach($funciones as $funcion){

                        $entradas = getEntradasListPorFuncion($funcion['funcion_id']);
                        // echo "//////////////////<br>";
                        // print_r($funcion);
                        // echo "-------------<br>";
                        // print_r($entradas);
                        // foreach($entradas as $entrada){
                            
                        //     // echo "-------------<br>";
                        //     // print_r($entrada);
                        //     // echo "-------------<br>";
                        //     if($entrada['orden_aceptada'] == 0){
                        //         $entrada_cantidad ++;
                        //     }
                        // }?>
                <?php } ?>
            </div>
        </a>
    <?php
    }
    ?>
    </div>
</div>

<?php include "../inc/template/escritorio_footer.php";?>

<script>
    $('#carousel-inner div:first-child').addClass('active');
</script>