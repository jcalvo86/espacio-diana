<?php   
    include '../inc/template/escritorio_navbar.php';
    include '../inc/php/actividades_list.php';

    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $obras = getActividadesListAll($id);
        $obras_fetch = $obras->fetch_assoc();
        $funciones = getFuncionesListForActividad($obras_fetch['actividad_id']);
        $entradas = getEntradasListAll();
    }else{
        $obras = getActividadesListAll();
        $funciones = getFuncionesListAll();
        $entradas = getEntradasListAll();
    }

?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="../index_main.php">Inicio</a></li>
    <li class="breadcrumb-item"><a href="index.php">Escritorio</a></li>
    <li class="breadcrumb-item active" aria-current="page"><a>Funciones</a></li>
  </ol>
</nav>

<!-- INFORMACION -->

<!-- FUNCIONES -->



<?php foreach($obras as $obra){
    echo "<h4>".$obra['actividad_nombre'] ." / " . $obra['actividad_fechas']."</h4>";
    echo "<br>";

    ?>
    <table class="table">
  <thead>
    <tr>
      <th scope="col">Fecha</th>
      <th scope="col">Entradas Adquiridas</th>
      <th scope="col">Monto Recolectado</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>

    <?php

    foreach($funciones as $funcion){
        if($funcion['funcion_actividad_id'] == $obra['actividad_id']){
            $cantidad_entradas = 0;
            $monto_recolectado = 0;

            foreach($entradas as $entrada){
                if($entrada['entrada_funcion_id'] == $funcion['funcion_id']){
                    $cantidad_entradas = $cantidad_entradas + 1;
                    $monto_recolectado = $monto_recolectado + $funcion['funcion_monto_recolectado'];
                }
            }
            
            if( $cantidad_entradas != 0){
                ?>
                <tr>
                    <th scope="row"><?php echo displayFecha($funcion['funcion_dia']); ?></th>
                    <td><?php echo $cantidad_entradas ?></td>
                    <td><?php echo "$".$funcion['funcion_monto_recolectado'] ?></td>
                    <td><?php echo "<a href='entradas.php?id=".$funcion['funcion_id'] ."'>"."Ir"."</a>"; ?></td>
                </tr>
                <?php
            }
        }
    }
    echo "<hr>";

    ?>
      </tbody>
    </table>
    <?php
} ?>



</div>

<?php include "../inc/template/escritorio_footer.php";?>

<script>
    $('#carousel-inner div:first-child').addClass('active');
</script>