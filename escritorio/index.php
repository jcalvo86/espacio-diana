<?php   
    include '../inc/template/escritorio_navbar.php';
    include '../inc/php/actividades_list.php';

    if (isset($_GET['id'])) {
        $id = $_GET['id'];
    }
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index_main.php">Inicio</a></li>
    <li class="breadcrumb-item active" aria-current="page"><a>Escritorio</a></li>
  </ol>
</nav>


<?php include "../inc/template/escritorio_footer.php";?>

<script>
    $('#carousel-inner div:first-child').addClass('active');
</script>