<?php   
    include '../inc/template/escritorio_navbar.php';
    include '../inc/php/actividades_list.php';

    if (isset($_GET['id'])) {
        $id = $_GET['id'];
    }


    $funcion = getFuncionInfo($id);

    $actividad = getActividadInfo($id);
    $entradas = getEntradasListPorFuncion($id);
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="../index_main.php">Inicio</a></li>
    <li class="breadcrumb-item"><a href="index.php">Escritorio</a></li>
    <li class="breadcrumb-item"><a href="funciones.php?id=<?php echo $actividad['actividad_id']?>"><?php echo text_capitalize($actividad['actividad_nombre'])?></a></li>
    <li class="breadcrumb-item active" aria-current="page">Entradas</li>
  </ol>
</nav>
<h3><?php echo $funcion['actividad_nombre']?> <?php if($funcion['funcion_pago'] != "archivo"){ echo $funcion['funcion_dia']; } else{ echo " - Archivo";} ?></h3>
<h4>espaciodiana.cl/funcion.php?funcion_id=<?php echo $id ?></h4>
<hr>
<?php 
$entradas_totalcount=0;
$entradas_totalcollected=0;
foreach($entradas as $entrada){
  // print_r($entrada);
  if($entrada['orden_aceptada']==1 && $entrada['orden_token']!= 'invitacion'){
    $entradas_totalcount = $entradas_totalcount + 1;
    $entradas_totalcollected = $entradas_totalcollected + $entrada['entrada_pagado'];
  }
  
}?>
<h4><?php echo $entradas_totalcount;?> Entradas Aceptadas <span class=>(<?php echo $entradas_totalcollected;?> Pesos)</span> </h4>

<table class="table">
  <thead>
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Código</th>
      <th scope="col">Email</th>
      <th scope="col">Monto</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($entradas as $entrada){
    if($entrada['orden_aceptada']==1 && $entrada['orden_token']!= 'invitacion'){?>
    <tr>
      <th scope="row"><?php echo $entrada['entrada_nombre'] ?></th>
      <td><?php echo $entrada['entrada_code'] ?></td>
      <td><?php echo $entrada['entrada_email'] ?></td>
      <td><?php echo $entrada['entrada_pagado'] ?></td>
    </tr>
    <?php }
  } ?>

  </tbody>
</table>


<hr>
<br>
<br>
<h4>Invitaciones</h4>

<table class="table">
  <thead>
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Código</th>
      <th scope="col">Email</th>
      <th scope="col">Monto</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($entradas as $entrada){
    if($entrada['orden_aceptada']==1 && $entrada['orden_token']== 'invitacion'){?>
    <tr>
      <th scope="row"><?php echo $entrada['entrada_nombre'] ?></th>
      <td><?php echo $entrada['entrada_code'] ?></td>
      <td><?php echo $entrada['entrada_email'] ?></td>
      <td><?php echo $entrada['entrada_pagado'] ?></td>
    </tr>
    <?php }
  } ?>

  </tbody>
</table>

<hr>
<br>
<br>
<h4>Entradas No Vendidas</h4>
<table class="table">
  <thead>
    <tr>
      <th scope="col">Nombre</th>
      <th scope="col">Código</th>
      <th scope="col">Email</th>
      <th scope="col">Monto</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($entradas as $entrada){
    if($entrada['orden_aceptada']==0){?>
    <tr>
      <th scope="row"><?php echo $entrada['entrada_nombre'] ?></th>
      <td><?php echo $entrada['entrada_code'] ?></td>
      <td><?php echo $entrada['entrada_email'] ?></td>
      <td><?php echo $entrada['entrada_pagado'] ?></td>
    </tr>
    <?php }
  } ?>

  </tbody>
</table>
<hr>
<hr>
<br>
<br>
<h4>Nueva Entrada</h4>
<form id="generarEntradaNueva">
  <div class="form-group">
    <label for="exampleFormControlInput1">Nombre</label>
    <input type="text" class="form-control" id="nombre" placeholder="Diana Prince">
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Email</label>
    <input type="email" class="form-control" id="email" placeholder="diana@espaciodiana.cl">
  </div>
  
  <div class="form-group">
    <label for="exampleFormControlSelect1">Tipo de entrada</label>
    <select class="form-control" id="entrada_tipo">
      <option>Entrada Comprada</option>
      <option>Invitación</option>
    </select>
  </div>

  <input type="number" class="form-control" id="funcion_id" value="<?php echo $id?>" hidden>
  <button type="submit" class="btn btn-primary">Enviar</button>
</form>

</div>

<?php include "../inc/template/escritorio_footer.php";?>

<script>
    $('#carousel-inner div:first-child').addClass('active');
</script>