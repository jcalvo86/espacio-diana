<?php   
    include '../inc/template/escritorio_navbar.php';
    include '../inc/php/actividades_list.php';

    $obras = getActividadesListAll();

    $entradas = getEntradasListAll();
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="../index_main.php">Inicio</a></li>
    <li class="breadcrumb-item"><a href="index.php">Escritorio</a></li>
    <li class="breadcrumb-item active" aria-current="page"><a>Administrador</a></li>
  </ol>
</nav>

<div class="card__container">
    <div class="card_horizontal flex-1">
        <button onclick="recalcularDatosDeTodasActividades()">Recalcular</button>
    </div>
</div>

<?php include "../inc/template/escritorio_footer.php";?>

<script>
    $('#carousel-inner div:first-child').addClass('active');
</script>