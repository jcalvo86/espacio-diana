<?php   
    include '../inc/template/escritorio_navbar.php';
    include '../inc/php/actividades_list.php';

    $obras = getActividadesListAll();

    $entradas = getEntradasListAll();
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="../index_main.php">Inicio</a></li>
    <li class="breadcrumb-item"><a href="index.php">Escritorio</a></li>
    <li class="breadcrumb-item active" aria-current="page"><a>Invitaciones</a></li>
  </ol>
</nav>


<div class="card__container--list">
    <?php foreach($obras as $obra){
        ?>

        <div class="card_horizontal flex-1" href="actividad.php?id=<?php echo $obra['actividad_id']?>">
            <div class="card_description flex-1">
                <h3><?php echo $obra['actividad_nombre']?></h3>
                <p><?php echo $obra['actividad_fechas'];?></p>
                <hr>

                <?php 
                    $funciones = getFuncionesListForActividad($obra['actividad_id']);
                    $entrada_cantidad = 0;
                    foreach($funciones as $funcion){
                        
                      echo $funcion['funcion_dia']. " espaciodiana.cl/funcion.php?funcion_id=".$funcion['funcion_id'];
                      echo "<br>";
                        
                      $entradas = getEntradasListPorFuncion($funcion['funcion_id']);

                        foreach($entradas as $entrada){
                            if($entrada['orden_token'] == "invitacion" && $entrada['entrada_funcion_id'] == $funcion['funcion_id']){
                              echo $entrada['entrada_nombre'] . " / ". $entrada['entrada_code'];
                              echo "<br>";
                            }
                        }
                        echo "<br>";
                        ?>
                <?php } ?>
            </div>
        </div>
    <?php
    }
    ?>
    </div>
</div>

<?php include "../inc/template/escritorio_footer.php";?>

<script>
    $('#carousel-inner div:first-child').addClass('active');
</script>