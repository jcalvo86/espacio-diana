<?php   
    include 'inc/template/navbar.php';
    include 'inc/php/actividades_list.php';

    $noticias = getNoticiasList();
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index_main.php">Inicio</a></li>
    <li class="breadcrumb-item active" aria-current="page">Noticias</li>
  </ol>
</nav>

<div class="contenedor">


    <div class="segment_proximos_title layout-box">
        <h3>Convocatorias</h3>
        <hr>
        <div class="segment_proximos_eventos flex-wrap">
            <?php 
            foreach($noticias as $noticia){ ?>
            <a class="card flex-30" href="noticia.php?id=<?php echo $noticia['convocatoria_id']?>">
                <div class="card_image" style="background-image:url(files/convocatoria/<?php echo $noticia['convocatoria_imagen']?>);">
                    <div class="card_image_type"> <?php echo text_capitalize("Convocatoria")?> | <?php echo display_date_MMYYYY($noticia['convocatoria_fecha'])?> </div>
                </div>

                <div class="card_description">
                    <h4><?php echo $noticia['convocatoria_nombre']?></h4>
                </div>
            </a>
            <?php } ?>
        </div>
    </div>





</div>

<?php include "footer.php";?>

<script>
    $('#carousel-inner div:first-child').addClass('active');
</script>