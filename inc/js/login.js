eventListeners();

console.log("running login_forms");

function eventListeners() {
    if (document.querySelector("#login")) {
        document.querySelector("#login").addEventListener("submit", validateLogin);
    }
}

function addPassword(e) {
    e.preventDefault();
    var login_user = document.getElementById("login_user"),
        login_password = document.getElementById("login_password"),
        confirm_password = document.getElementById("confirm_password"),
        code = document.getElementById("code").value;

    if (login_user.value === "") {
      login_user.focus();
      toastr.error("Debe ingresar el email asociado a esta cuenta");

    } else if (login_password.value === "") {
      login_password.focus();
      toastr.error("Debe ingresar un password para su usuario");
    } else if (confirm_password.value === "") {
      confirm_password.focus();
      toastr.error("Debe verificar el password para su usuario");
    } else {
        if (login_password.value != confirm_password.value) {
            toastr.error("Los Passwords no coinciden");
        } else {
            var data = new FormData();
            data.append("login_user", login_user.value);
            data.append("login_password", login_password.value);
            data.append("code", code);
            data.append("action", 'add-password');

            var xhr = new XMLHttpRequest();
            xhr.open("POST", "inc/php/users_model.php", true);
            xhr.onload = function() {
                if (this.status === 200) {
                    console.log(xhr);
                    var response = JSON.parse(xhr.responseText);
                    console.log(JSON.parse(xhr.responseText));
                    if (response.state === "correct") {
                        var toastHTML = "Su password ha sido creado exitosamente.";
                        toastr.options.onHidden = function() { window.location.href = "index.php"; };
                        toastr.success(toastHTML);
                    } else {
                        toastr.error(response.text);
                    }
                }
            };
            xhr.send(data);
        }
    }
}

function validateLogin(e) {
    e.preventDefault();

    var user_email = document.getElementById("login_user"),
        user_password = document.getElementById("login_password");

    if (user_email.value === "") {
        displayToast('alert','Debe ingresar su usuario');
        user_email.focus();

    } else if (user_password.value === "") {
        displayToast('alert','Debe ingresar el password para su usuario');
        user_password.focus();

    } else {
        console.log("USER AND PASSWORD");
        var data = new FormData();
        data.append("user_email", user_email.value);
        data.append("user_password", user_password.value);
        data.append("action", 'login');

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "inc/php/login_model.php", true);
        xhr.onload = function() {
            if (this.status === 200) {
                var response = JSON.parse(xhr.responseText);
                if (response.state === "correct") {
                    displayToast('correct','Bienvenido ' + response.name, "escritorio/index.php");
                } else {
                    displayToast('error','No tiene acceso a esta cuenta');
                }
            }
        };
        xhr.send(data);
    }
}