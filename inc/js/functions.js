

function displayError(element, text){
    element.classList.remove("display-hide");
    element.classList.add("invalid");
    element.classList.add("display-show");
    element.innerHTML = text;
}

function displayCorrect(element, text){
    element.classList.remove("display-hide");
    element.classList.add("valid");
    element.classList.add("display-show");
    element.innerHTML = text;
}

function displayClear(element){
    element.classList.add("display-hide");
    element.innerHTML = '';
}

function displayToast(type, message, redirect = null){

    $('.toast').toast('show');

    toasttitle = document.getElementById("toast-title");
    if(type=='correct'){
        toasttitle.innerHTML = '<i class="fas fa-check"></i>';
    }else if(type=='alert'){
        toasttitle.innerHTML = '<i class="fas fa-exclamation-triangle"></i>';
    }else if(type=='error'){
        toasttitle.innerHTML = '<i class="fas fa-times"></i>';
    }

    toastmessage = document.getElementById("toast-message");
    toastmessage.innerHTML = message;

    if(redirect != null){
        $('.toast').on('hidden.bs.toast', function () {
            window.location.href = redirect;
          })
    }

}

// ENTRADAS
function reenviarEmailEntrada($entrada_id){
    console.log("reenviando");

    var data = new FormData();
    data.append("entrada_id",$entrada_id);
    data.append("action", "reenviarEmailEntrada");

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../inc/php/entrada_model.php", true);
    xhr.onload = function() {
        if (this.status === 200) {
            console.log(this);
            var response = JSON.parse(xhr.responseText);
            console.log(response);
            if (response.state === "correct") {
                displayToast("correct", "Su entrada ha sido adquirida correctamente");
            } else {
                displayToast("error", response.text);
            }
        }
    };
    xhr.send(data);
}

//FUNCIONES
function recalcularEntradasAceptadasParaFuncion(funcion_id){

    console.log("recalcularEntradasAceptadasParaFuncion");

    var data = new FormData();
    data.append("funcion_id",funcion_id);
    data.append("action", "recalcularEntradasAceptadasParaFuncion");

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../inc/php/listener.php", true);
    xhr.onload = function() {
        if (this.status === 200) {
            console.log(this);
            var response = JSON.parse(xhr.responseText);
            console.log(response);
            if (response.state === "correct") {
                displayToast("correct", "Su entrada ha sido adquirida correctamente");
            } else {
                displayToast("error", response.text);
            }
        }
    };
    xhr.send(data);
}

//==================OBRAS
function recalcularDatosDeTodasActividades(){

    console.log("recalcularDatosDeTodasActividades");

    var data = new FormData();
    data.append("action", "recalcularDatosDeTodasActividades");

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../inc/php/listener.php", true);
    xhr.onload = function() {
        if (this.status === 200) {
            console.log(this);
            if (this.state === "correct") {
                displayToast("correct", "Se ha recalculado todo");
            } else {
                displayToast("error", this.text);
            }
        }
    };
    xhr.send(data);
}