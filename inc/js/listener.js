console.log("running listener");

eventListeners();

function eventListeners() {
    if (document.getElementById("pregunta_form")) {
        document.getElementById("pregunta_form")
            .addEventListener("submit", pregunta_upload);
    }
    if (document.getElementById("updateEmail_form")) {
        document.getElementById("updateEmail_form")
            .addEventListener("submit", updateEmail);
    }
    if (document.getElementById("validarEntrada_form")) {
        document.getElementById("validarEntrada_form")
            .addEventListener("submit", validar_entrada);
    }
    if (document.getElementById("realizarReserva_form")) {
        document.getElementById("realizarReserva_form")
            .addEventListener("submit", realizar_reserva);
    }
    if (document.getElementById("generarEntradaNueva")) {
        document.getElementById("generarEntradaNueva")
            .addEventListener("submit", generarEntradaNueva);
    }
}

entrada_revalidar_id = "0";


function pregunta_upload(e) {
    e.preventDefault();
    console.log("CORRIENDO");

    var data = new FormData();
    var action = "pregunta_upload";
    var pregunta_respuesta = document.getElementById("pregunta_respuesta");
    var pregunta_usuario = document.getElementById("pregunta_usuario");

    if (pregunta_respuesta.value == "") {

        $('#pregunta_respuesta').popover('show');


    } else if(pregunta_usuario.value == ""){
        $('#pregunta_usuario').popover('show');
    }else{

        data.append("pregunta_respuesta", pregunta_respuesta.value);
        data.append("pregunta_usuario", pregunta_usuario.value);
        data.append("action", action);

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "./inc/php/pregunta_model.php", true);
        xhr.onload = function() {
            if (this.status === 200) {
                var response = JSON.parse(xhr.responseText);

                if (response.state == "correct") {
                    $('#nombre_usuario').text(response.nombre);
                    $('#id_usuario').val(response.inserted_id);

                    $('#modalNewsletter').modal('toggle');

                    var toastHTML = response.text;
                    console.log(toastHTML);
                    return;

                } else {
                    console.log("Error");
                    var toastHTML = response.text;
                    console.log(toastHTML);
                    
                    return;
                }
            }
        };
        xhr.send(data);
    }
}

function updateEmail(e) {
    e.preventDefault();
    console.log("CORRIENDO");

    var data = new FormData();
    var action = "updateEmail";
    var email_usuario = document.getElementById("email_usuario");
    var id_usuario = document.getElementById("id_usuario");

    if (email_usuario.value == "") {

        $('#email_usuario').popover('show');

    }else{

        data.append("email_usuario", email_usuario.value);
        data.append("id_usuario", id_usuario.value);
        data.append("action", action);

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "./inc/php/pregunta_model.php", true);
        xhr.onload = function() {
            if (this.status === 200) {
                var response = JSON.parse(xhr.responseText);

                if (response.state == "correct") {

                    $('#modalNewsletter').modal('toggle');
                    $('#modalSuccess').modal('toggle');

                    var toastHTML = response.text;
                    console.log(toastHTML);
                    return;

                } else {
                    console.log("Error");
                    var toastHTML = response.text;
                    console.log(toastHTML);
                    
                    return;
                }
            }
        };
        xhr.send(data);
    }
}

function validar_entrada(e) {
    e.preventDefault();

    console.log("INN");
    
    var user_codigo = document.getElementById("user_codigo"),
        user_codigo_valid = document.getElementById("user_codigo_valid"),
        user_valid = document.getElementById("user_valid"),
        funcion_id = document.getElementById("funcion_id"),

    user_codigo_isValid = 0;
    if (user_codigo.value === "") {
        displayError(user_codigo_valid,"Debe ingresar el código para su usuario");
    }else{
        displayClear(user_codigo_valid);
        user_codigo_isValid = 1;
    };

    if(user_codigo_isValid == 1) {
        var data = new FormData();
        data.append("user_codigo", user_codigo.value);
        data.append("funcion_id", funcion_id.value);
        data.append("action", "validar_entrada");

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "inc/php/validacion_model.php", true);
        xhr.onload = function() {
            if (this.status === 200) {
                console.log("200");
                console.log(this);
                var response = JSON.parse(xhr.responseText);
                console.log(JSON.parse(xhr.responseText));
                if (response.state === "correct") {
                    displayCorrect(user_valid, "Su código ha sido validado correctamente, en breve podrá ver su función.");
                    entrada_revalidar_id = response.entrada_id;
                    setInterval(
                        function(){ $('#funcionModal').modal('hide') },
                        2000
                      );
                } else {
                    displayError(user_valid, response.text);
                }
            }
        };
        xhr.send(data);
    }
}

function revalidar_entrada(){
    if(entrada_revalidar_id != "0"){
        console.log(entrada_revalidar_id);
        var data = new FormData();
        data.append("entrada_id", entrada_revalidar_id);
        data.append("action", "revalidar_entrada");

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "inc/php/validacion_model.php", true);
        xhr.send(data);
    }
}

// RESERVAS

function realizar_reserva(e) {
    e.preventDefault();

    console.log("REALIZANDO RESERVA");
    
    var user_email = document.getElementById("email").value;

    var isvalid = true;
    if(user_email == ""){
        isvalid = false;
        displayToast("correct", "Su reserva ha sido relizada correctamente");
    }


    if(document.getElementById("entradas")){
        var entrada_cant = document.getElementById("entradas").value; 
    }else{
        var entrada_cant = 1;
    }
    var user_nombre0 = document.getElementById("nombre0").value;
    var user_nombre1 = "";
    var user_nombre2 = "";
    var user_nombre3 = "";
    var user_nombre4 = "";
    var funcion_id = document.getElementById("funcion_id").value;

    if(entrada_cant >= 2){
        var user_nombre1 = document.getElementById("nombre1").value;
    }
    if(entrada_cant >= 3){
        var user_nombre2 = document.getElementById("nombre2").value;
    }
    if(entrada_cant >= 4){
        var user_nombre3 = document.getElementById("nombre3").value;
    }
    if(entrada_cant >= 5){
        var user_nombre4 = document.getElementById("nombre4").value;
    }
    console.log("entradas:"+entrada_cant);
    console.log(user_nombre0+","+user_nombre1+","+user_nombre2+","+user_nombre3+","+user_nombre4);

    if(document.getElementById("nombre_nino")){
        var nombre_nino = document.getElementById("nombre_nino").value; 
    }else{
        var nombre_nino = "";
    }

    if(document.getElementById("telefono")){
        var telefono = document.getElementById("telefono").value; 
    }else{
        var telefono = 0;
    }

    if(isvalid == true){
        var data = new FormData();
        data.append("user_email", user_email);
        data.append("entrada_cant", entrada_cant);
        data.append("user_nombre0", user_nombre0);
        data.append("user_nombre1", user_nombre1);
        data.append("user_nombre2", user_nombre2);
        data.append("user_nombre3", user_nombre3);
        data.append("user_nombre4", user_nombre4);
        data.append("funcion_id", funcion_id);
        data.append("nombre_nino", nombre_nino);
        data.append("telefono", telefono);
        data.append("action", "realizar_reserva");

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "inc/php/reserva_model.php", true);
        xhr.onload = function() {
            if (this.status === 200) {
                console.log(this);
                var response = JSON.parse(xhr.responseText);
                console.log(response);
                if (response.state === "correct") {
                    displayToast("correct", "Su reserva ha sido realizada correctamente", "reserva_confirmacion.php?funcion="+response.funcion+"&code="+response.code);
                } else {
                    displayToast("error", response.text);
                }
            }
        };
        xhr.send(data);
    }

}

//ENTRADAS
function generarEntradaNueva(e){
    e.preventDefault();

 nombre = document.getElementById("nombre").value;
 email = document.getElementById("email").value;
 funcion_id = document.getElementById("funcion_id").value;
 entrada_tipo = document.getElementById("entrada_tipo").value;

is_valid=true;

 if(nombre == ""){
    is_valid =false;
    displayToast("error", "El nombre es válido");

 }else if(email ==""){
    is_valid =false;
    displayToast("error", "El email no es válido");

 }else{

    var data = new FormData();
    data.append("nombre", nombre);
    data.append("email", email);
    data.append("funcion_id", funcion_id);
    data.append("entrada_tipo", entrada_tipo);
    data.append("action", "generarEntradaNueva");

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../inc/php/entrada_model.php", true);
    xhr.onload = function() {
        if (this.status === 200) {
            console.log(this);
            var response = JSON.parse(xhr.responseText);
            console.log(response);
            if (response.state === "correct") {
                displayToast("correct", "Su entrada ha sido adquirida correctamente", "entradas.php?id="+funcion_id);
            } else {
                displayToast("error", response.text);
            }
        }
    };
    xhr.send(data);
 }

}