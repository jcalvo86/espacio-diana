<?php

function get_preguntas_list(){
    include 'connection.php';
    try {
      $preguntas =  $conn->query("SELECT * FROM pregunta_hoy 
      ORDER BY preg_id DESC
      LIMIT 24");

        $preguntas_text = array();

        foreach($preguntas as $pregunta){
            array_push($preguntas_text, $pregunta);
        }

      return $preguntas_text;
    } catch (Exception $e) {
      echo "Error! : " . $e->getMessage();
      return false;
    }
  }

  function   get_preguntas_list_parafondo(){
    include 'connection.php';
    try {
      $preguntas =  $conn->query("SELECT * FROM pregunta_hoy 
      ORDER BY preg_id DESC
      LIMIT 24");

        $preguntas_text = array();

        foreach($preguntas as $pregunta){
            array_push($preguntas_text, $pregunta['preg_texto']);
        }

      return $preguntas_text;
    } catch (Exception $e) {
      echo "Error! : " . $e->getMessage();
      return false;
    }
  }

  function   get_preguntas_amount(){
    include 'connection.php';
    try {
      $preguntas =  $conn->query("SELECT COUNT(*)
      FROM pregunta_hoy");
      $preguntas = $preguntas->fetch_assoc();
      $preguntas = $preguntas['COUNT(*)'];
      
      return $preguntas;
    } catch (Exception $e) {
      echo "Error! : " . $e->getMessage();
      return false;
    }
  }



  function get_pregunta_last(){
    include 'connection.php';
    try {
      $preguntas =  $conn->query("SELECT * FROM pregunta_hoy 
      ORDER BY preg_id DESC
      LIMIT 1");

      return $preguntas->fetch_assoc();
    } catch (Exception $e) {
      echo "Error! : " . $e->getMessage();
      return false;
    }
  }