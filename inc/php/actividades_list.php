<?php

function getActividadesListForMes($año, $mes){

    $fecha_inicio =$año."-".$mes."-01";
    $fecha_final =$año."-".$mes."-30";

    // $query = "SELECT * FROM actividades
    // WHERE act_inicio >= '$fecha_inicio' 
    // AND act_final <= '$fecha_final'";

    $query = "SELECT * FROM actividades";

    echo $query;
    echo "<br><br>";

    include 'connection.php';
    return $conn->query($query);
}

function getActividadesListAll($id = null){

    if($id == null){
        $query = "SELECT * FROM actividades";
    }else{
        $query = "SELECT * FROM actividades WHERE actividad_id = $id";
    }

    include 'connection.php';
    return $conn->query($query);
}

function getActividadesListFamilia($cuando = "all"){

    $query = "SELECT * FROM actividades ";
    $query .= "WHERE is_active != 0 ";
    $query .= "AND actividad_is_familiar = 1 ";

    //$cuando tiene 3 estados 
    //"All" = todos, 
    //"next" = los que tienen fecha de cierre despues de la fecha actual, 
    //"past" = los que tiene fecha de cierre anes de la fecha actual

    $hoy = date("Y/m/d"); 

    if($cuando == "next"){
        $query .= "AND actividad_final >= '$hoy' ";
    }else if($cuando == "past"){
        $query .= "AND actividad_final < '$hoy' ";
    }


    //echo $query;
    // echo "<br><br>";

    include 'connection.php';
    $result = $conn->query($query);
    if($result){
        return $result;
    }
}

function getActividadesListProximos($cant, $tipo = 'all', $cuando = "all", $state = "active", $other = ""){

    $query = "SELECT * FROM actividades";
    $query .= " WHERE is_active != 0 ";

    //

    if($tipo != 'all'){
        $query .= " AND actividad_tipo = '".$tipo."'";
    }

    //$CUANDO tiene 3 estados 
    //"All" = todos, 
    //"next" = los que tienen fecha de cierre despues de la fecha actual, 
    //"past" = los que tiene fecha de cierre anes de la fecha actual

    $hoy = date("Y/m/d"); 

    if($cuando == "next"){
        $query .= " AND actividad_final >= '$hoy' ";
    }else if($cuando == "past"){
        $query .= " AND actividad_final < '$hoy' ";
    }

    //$STATE
    //0 = No activa
    //1 = Activa, pero sin programa
    //2 = Programada
    //3 = En funciones actualmente
    //4 = Terminado
    if($state == "active"){
        $query .= " AND actividad_state >= 2 ";
        $query .= " AND actividad_state <= 3 ";
    }else if($state == "past"){
        $query .= " AND actividad_state >= 3 ";
    }else if($state == "ended"){
        $query .= " AND actividad_state = 4 ";
    }else if($state == "all"){
        $query .= " AND actividad_state >= 1 ";
    }else if($state == "soon"){
        $query .= " AND actividad_state = 1 ";
    }
    //

    if($other != ""){
        if($other = "familiar"){
            $query .= " AND actividad_is_familiar = 1 "; 
        }

    }

    

    $query .= " LIMIT $cant";
    
    // echo $query;
    
    include 'connection.php';
    $result = $conn->query($query);
    if($result){

        return $result;
    }
}

function getActividadInfo($id){
    include 'connection.php';
    $result = $conn->query("SELECT * FROM actividades 
                            JOIN realizadores ON actividades.actividad_realizador_id = realizadores.realizador_id
                            WHERE actividad_id = '$id'");
    if($result){
        if($result->num_rows === 0){
            return null;
        }else{
            return $result->fetch_assoc();;
        }
    }
}

function getEntradaInfoByToken($token){
    include 'connection.php';
    $result = $conn->query("SELECT * FROM entradas 
                            JOIN funciones ON funciones.funcion_id = entradas.entrada_funcion_id
                            JOIN actividades ON actividades.actividad_id = funciones.funcion_actividad_id
                            WHERE orden_token = '$token'");
    if($result){
        if($result->num_rows === 0){
            return null;
        }else{
            return $result->fetch_assoc();
        }
    }
}

function getActividadFechas($id){

    $dia = date("yy-m-d");

    include 'connection.php';
    $query = "SELECT * FROM funciones 
    WHERE funcion_actividad_id = '$id'
    AND funcion_dia >= '$dia'
    AND funcion_estado = 1
    ORDER BY funcion_dia";
    $result = $conn->query($query);
    // echo $query;
    if($result){
        if($result->num_rows === 0){
            return null;
        }else{
            return $result;
        }
    }
}

//NOTICIAS
function getNewsList($cant = "all"){

    $query = "SELECT * FROM noticias";

    if($cant != "all"){
        $query .= " LIMIT $cant";
    }
    
    // echo $query;
    include 'connection.php';
    $result = $conn->query($query);
    if($result){

        return $result;
    
    }
}


function getFuncionesListForMes($año, $mes){

    $fecha_inicio =$año."-".$mes."-01";
    $fecha_final =$año."-".$mes."-30";

    // $query = "SELECT * FROM actividades
    // WHERE act_inicio >= '$fecha_inicio' 
    // AND act_final <= '$fecha_final'";

    $query = "SELECT * FROM funciones
    JOIN actividades ON actividades.actividad_id = funciones.funcion_actividad_id
    WHERE funcion_fecha >= '$fecha_inicio'
    AND funcion_fecha <= '$fecha_final'";

    echo $query;
    echo "<br><br>";

    include 'connection.php';
    return $conn->query($query);
}

function getFuncionesListAll(){


    $query = "SELECT * FROM funciones
    JOIN actividades ON actividades.actividad_id = funciones.funcion_actividad_id";

    // echo $query;
    // echo "<br><br>";

    include 'connection.php';
    return $conn->query($query);
}

function getFuncionesListForActividad($actividad_id){

    $query = "SELECT * FROM funciones
    JOIN actividades ON actividades.actividad_id = funciones.funcion_actividad_id
    WHERE funcion_actividad_id = '$actividad_id'";

    include 'connection.php';
    return $conn->query($query);
}

function getFuncionInfo($id){
    include 'connection.php';

    $query = "SELECT * FROM funciones JOIN actividades ON actividades.actividad_id = funciones.funcion_actividad_id WHERE funcion_id = '$id'";
    //echo $query;

    $result = $conn->query($query);
    if($result){
        if($result->num_rows === 0){
            return null;
        }else{
            return $result->fetch_assoc();;
        }
    }
}

function getFuncionInfoPorToken($id){
    include 'connection.php';
    $result = $conn->query("SELECT * FROM entradas 
                            JOIN funciones ON entradas.entrada_funcion_id = funciones.funcion_id
                            JOIN actividades ON actividades.actividad_id = funciones.funcion_actividad_id
                            WHERE entradas.orden_token = '$id'");
    if($result){
        if($result->num_rows === 0){
            return null;
        }else{
            return $result->fetch_assoc();;
        }
    }
}

function getFuncionInfoPorFuncionIdAndCode($funcion_id, $code){
    include 'connection.php';
    $query = "SELECT * FROM entradas 
    JOIN funciones ON entradas.entrada_funcion_id = funciones.funcion_id
    JOIN actividades ON actividades.actividad_id = funciones.funcion_actividad_id
    WHERE entradas.entrada_funcion_id = '$funcion_id'
    AND entradas.entrada_code = '$code'";

    $result = $conn->query($query);

    if($result){
        if($result->num_rows === 0){
            return null;
        }else{
            return $result;
        }
    }
}

function getEntradaListPorToken($token){
    include 'connection.php';
    $result = $conn->query("SELECT * FROM entradas 
                            JOIN funciones ON entradas.entrada_funcion_id = funciones.funcion_id
                            JOIN actividades ON actividades.actividad_id = funciones.funcion_actividad_id
                            WHERE entradas.orden_token = '$token'");
    if($result){
        if($result->num_rows === 0){
            return null;
        }else{
            return $result;
        }
    }
}

function getEntradaInfoPorToken($token){
    include 'connection.php';
    $result = $conn->query("SELECT * FROM entradas 
                            JOIN funciones ON entradas.entrada_funcion_id = funciones.funcion_id
                            JOIN actividades ON actividades.actividad_id = funciones.funcion_actividad_id
                            WHERE entradas.orden_token = '$token'");
    if($result){
        if($result->num_rows === 0){
            return null;
        }else{
            return $result->fetch_assoc();
        }
    }
}

function getEntradasListPorFuncion($funcion_id){
    include 'connection.php';
    $query = "SELECT * FROM entradas 
    JOIN funciones ON entradas.entrada_funcion_id = funciones.funcion_id
    JOIN actividades ON actividades.actividad_id = funciones.funcion_actividad_id
    WHERE entradas.entrada_funcion_id = '$funcion_id'
    ";

    $result = $conn->query($query);
    if($result){
        return $result;
    }
}

function getEntradasListAll(){
    include 'connection.php';
    $result = $conn->query("SELECT * FROM entradas 
                            JOIN funciones ON entradas.entrada_funcion_id = funciones.funcion_id
                            JOIN actividades ON actividades.actividad_id = funciones.funcion_actividad_id");
    if($result){
        if($result->num_rows === 0){
            return null;
        }else{
            return $result;
        }
    }
}

function getEntradaInfo($entrada_id){
    include 'connection.php';
    $result = $conn->query("SELECT * FROM entradas 
                            JOIN funciones ON entradas.entrada_funcion_id = funciones.funcion_id
                            JOIN actividades ON actividades.actividad_id = funciones.funcion_actividad_id
                            WHERE entrada_id = '$entrada_id'");
    if($result){
        if($result->num_rows === 0){
            return null;
        }else{
            return $result->fetch_assoc();;
        }
    }
}


//COLABORADORES
function getColaboradoresList($cant = "all", $tipo ="all"){

    $query = "SELECT * FROM realizadores";
    $query .= " WHERE realizador_id != 0";   

    if($tipo != "all"){
        $query .= " AND realizador_tipo = '$tipo'";
    }
    
    if($cant != "all"){
        $query .= " LIMIT $cant";
    }

    //echo $query;
    include 'connection.php';
    $result = $conn->query($query);
    if($result){

        return $result;
    
    }
}

//NOTICIAS
function getNoticiasList($cant = "all", $tipo ="all"){

    $query = "SELECT * FROM convocatorias";
    $query .= " WHERE convocatoria_id != 0";   

    if($tipo != "all"){
        $query .= " AND realizador_tipo = '$tipo'";
    }
    
    if($cant != "all"){
        $query .= " LIMIT $cant";
    }

    //echo $query;
    include 'connection.php';
    $result = $conn->query($query);
    if($result){

        return $result;
    
    }
}

function getConvocatoriaInfo($id){
    include 'connection.php';
    $result = $conn->query("SELECT * FROM convocatorias 
                            WHERE convocatoria_id = '$id'");
    if($result){
        if($result->num_rows === 0){
            return null;
        }else{
            return $result->fetch_assoc();;
        }
    }
}


//Funciones

function getObraFichaInfo($id){
    include 'connection.php';
    $query = "SELECT * FROM obra_ficha WHERE obraficha_actividad_id = $id";
    // echo $query;
    $result = $conn->query($query);
    if($result){
        if($result->num_rows === 0){
            return null;
        }else{
            return $result->fetch_assoc();;
        }
    }
}

function displayFechasActividad($actividad){
    return $actividad;
}

function displayDia($actividad){
    $unixTimestamp = strtotime($actividad);
    $dayOfWeek = date("l", $unixTimestamp);
    
    if($dayOfWeek == "Monday"){
        return "Lunes";
    }else if($dayOfWeek == "Tuesday"){
        return "Martes";
    }else if($dayOfWeek == "Wednesday"){
        return "Miercoles";
    }else if($dayOfWeek == "Thursday"){
        return "Jueves";
    }else if($dayOfWeek == "Friday"){
        return "Viernes";
    }else if($dayOfWeek == "Saturday"){
        return "Sábado";
    }else if($dayOfWeek == "Sunday"){
        return "Domingo";
    }
}

function displayHora($actividad){
    return $actividad."hrs";
}

function displayFecha($actividad){

    $dia = displayDia($actividad);

    $date = explode("-", $actividad);
    $mes="";
    if($date[1] == "01"){
        $mes = "enero";
    }else if($date[1] == "02"){
        $mes = "febrero";
    }else if($date[1] == "03"){
        $mes = "marzo";
    }else if($date[1] == "04"){
        $mes = "abril";
    }else if($date[1] == "05"){
        $mes = "mayo";
    }else if($date[1] == "06"){
        $mes = "junio";
    }else if($date[1] == "07"){
        $mes = "julio";
    }else if($date[1] == "08"){
        $mes = "agosto";
    }else if($date[1] == "09"){
        $mes = "septiembre";
    }else if($date[1] == "10"){
        $mes = "octubre";
    }else if($date[1] == "11"){
        $mes = "noviembre";
    }else if($date[1] == "12"){
        $mes = "diciembre";
    }

    $return = $dia . " ". $date[2] ." de ". $mes;

    return $return;
}

function displayCantidadEntradas($entradas){
    if($entradas == 0){
        $text = "No hay entradas disponibles";
    } elseif ($entradas == 1){
        $text = "Última entrada disponible";
    }else{
        $text = $entradas." entradas disponible";
    }
    return $text;
}