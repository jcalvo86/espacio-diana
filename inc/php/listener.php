<?php

include 'funciones.php';

$action = $_POST['action'];

// ================== ENTRADAS

if ($action === 'recalcularEntradasAceptadasParaFuncion') {

    $action = $_POST['function_id'];
    recalcularEntradasAceptadasParaFuncion($function_id);
}

if ($action === 'recalcularTodasEntradasAceptadas') {

    recalcularTodasEntradasAceptadas();
}

if ($action === 'recalcularDatosDeTodasActividades') {

    recalcularDatosDeTodasActividades();
}