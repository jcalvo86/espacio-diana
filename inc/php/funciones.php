<?php

// TEXT
function text_trim($string, $max_length){
    if($max_length == ""){
        $max_length = 340;
    }

    if (strlen($string) > $max_length){
        $offset = ($max_length - 3) - strlen($string);
        $string = substr($string, 0, strrpos($string, ' ', $offset)) . '...';

    }
    return $string;
}

function text_capitalize($string){
    $sentences = preg_split('/([.?!]+)/', $string, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
    $new_string = '';
    foreach ($sentences as $key => $sentence) {
        $new_string .= ($key & 1) == 0?
            ucfirst(strtolower(trim($sentence))) :
            $sentence.' ';
    }
    return trim($new_string);
}


// DISPLAY
function display_date_MMYYYY($date){
    $date = explode("-", $date);
    $mes="";
    if($date[1] == "01"){
        $mes = "enero";
    }else if($date[1] == "02"){
        $mes = "febrero";
    }else if($date[1] == "03"){
        $mes = "marzo";
    }else if($date[1] == "04"){
        $mes = "abril";
    }else if($date[1] == "05"){
        $mes = "mayo";
    }else if($date[1] == "06"){
        $mes = "junio";
    }else if($date[1] == "07"){
        $mes = "julio";
    }else if($date[1] == "08"){
        $mes = "agosto";
    }else if($date[1] == "09"){
        $mes = "septiembre";
    }else if($date[1] == "10"){
        $mes = "octubre";
    }else if($date[1] == "11"){
        $mes = "noviembre";
    }else if($date[1] == "12"){
        $mes = "diciembre";
    }
    return $mes . " ". $date[0];
}

function displayIfExist($apendix ="", $data){
    if($data != ""){
        $return = "<p>";
        $return .= $apendix;
        $return .= $data;
        $return .= "</p>";

        return $return;
    }
}

function getPage(){
    $archivo = basename($_SERVER['PHP_SELF']);
    $page = str_replace(".php", "", $archivo);
    return $page;
}


// EMAIL
function enviarEmail($info){

    // $email_info = array(
    //     "email_action" => "reenviar_entrada",
    //     "from" => 'boleteria@espaciodiana.cl',
    //     "email" => $entrada['entrada_email'],
    //     "nombre" => $entrada['entrada_nombre'],
    //     "entrada_code" => $entrada['entrada_code'],
    //     "actividad_id" => $entrada['actividad_id'],
    //     "actividad_nombre" => $entrada['actividad_nombre'],
    //     "actividad_tipo" => $entrada['actividad_tipo'],
    //     "funcion_id" => $entrada['funcion_id'],
    //     "funcion_dia" => $entrada['funcion_dia'],
    //     "funcion_hora" => $entrada['funcion_hora'],
    //     "funcion_habilitado" => $entrada['funcion_habilitado'],
    //     "funcion_fin" => $entrada['funcion_fin'],
    //     "taller_fono" => $entrada['taller_fono'],
    //     "taller_nino" => $entrada['taller_nino']
    // );   

    if($info['email_action'] == "enviar_entrada" || $info['email_action'] == "reenviar_entrada" ){

        // ---------------------------------------------------------ENVIAR ENTRADAS & REENVIAR ENTRADAS
        $from = $info['from'];
        $to = $info['email'];
        $subject = 'entradas '.$info['actividad_nombre'];
    
        $headers = "De:" . $info['from'];
        $headers .= "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "Return-Path: Espacio Diana <contacto@espaciodiana.cl>\r\n"; 
        $headers .= "Organization: Espacio Diana\r\n";
        $headers .= 'From: Boleteria Espacio Diana <boleteria@espaciodiana.cl>' . "\r\n";
        
        $message ='';
    
        $message .='<!DOCTYPE html>';
        $message .='<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">';
        $message .='<head>';
    
        $message .='<style>';
        $message .='p {color:white !important; }';
        $message .='.mail{display:flex; flex-flow:row; }';
        $message .='.image{background-size:cover; background-position:center; width:40%;}';
        $message .='.content{width:100%; display:block; min-height:400px; font-size:16px; background-color:black; color:white; padding:10px;}';
        $message .='a {text-decoration:none; font-weight:bold; color:black; background-color:white; border-radius:10px; padding:5px;}'; 
        $message .='tr{ border-style: solid; border-color: white; border-width: 1px;}';
        $message .='table{ border-style: solid; border-color: white; border-width: 1px; width: 50%; text-align: center; margin-bottom:20px;}';
    
        $message .='</style>';
        $message .='</head>';
    
        $message .='<body>';
    
        if($info['actividad_tipo'] == "obra"){
    
            $message .='<div class="mail">';
            $message .='<div class="image" style="background-image:url(https://www.espaciodiana.cl/files/actividad/'.$info['actividad_id'].'/portada.jpg); "></div>';
            $message .='<div class="content">';
        
            $message .= "<h3>Entrada para ".$info['actividad_nombre']." </h3>";
            $message .= "<p>Hola ". $info['nombre'].", tus entradas para ".$info['actividad_nombre']." en su función de  ".$info['funcion_dia']." / ".$info['funcion_hora'].", fueron adquiridas correctamente.</p>";
        
            $message .= '<table class="table">';
            $message .= '<thead><tr><th scope="col">Nombre</th><th scope="col">Código</th></tr></thead><tbody>';
        
            $message .= '<tr><td>'.$info['nombre'].'</td><td>'.$info['entrada_code'].'</td></tr>';
            $message .= '</tbody></table>';
        
            $message .= "<a href='www.espaciodiana.cl/funcion.php?funcion_id=".$info['funcion_id']."' class='btn btn-rojo btn-fullwidth'>Ver Obra</a>" ;
            
            $message .= "<p> La obra quedará habilitada para que puedas presenciarla desde ".$info['funcion_habilitado']." a ".$info['funcion_fin']." horas del día de la función </p>" ;
            $message .= "<p> Ante cualquier duda puedes comunicarte al email contacto@espaciodiana.cl </p>" ;
        
            $message .= '</div>';
            $message .= '</div>';
    
        }else if($info['actividad_tipo'] == "taller"){
    
            $message .='<div class="mail">';
            $message .='<div class="image" style="background-image:url(https://www.espaciodiana.cl/files/actividad/'.$info['actividad_id'].'/portada.jpg); "></div>';
            $message .='<div class="content">';
        
            $message .= "<h3>Entrada para ".$info['actividad_nombre']." </h3>";
            $message .= "<p>".$info['nombre'].", tus entradas para ".$info['actividad_nombre']." están listas.";
            $message .= "<p>Te enviaremos las instrucciones para ingresar al taller proximamente.</p>" ;
            $message .= "<p>Recuerda que estas entradas son para el día de  ".$info['funcion_dia']." a las ".$info['funcion_hora']." horas.</p>";
            $message .= "<p>Ante cualquier duda puedes comunicarte al email contacto@espaciodiana.cl </p>" ;
    
            if($info['actividad_is_infantil']==1){
            $message .= '<table class="table">';
            $message .= '<thead><tr><th scope="col">Asistente</th></tr></thead><tbody>';
    
            $message .= '<tr><td>'.$info['taller_nino'].'</td></tr>';
    
            $message .= '</tbody></table>';
            }
    
            $message .= '</div>';
            $message .= '</div>';
    
        }else{
    
            $message .='<div class="mail">';
            $message .='<div class="image" style="background-image:url(https://www.espaciodiana.cl/files/actividad/'.$info['actividad_id'].'/portada.jpg); "></div>';
            $message .='<div class="content">';
        
            $message .= "<h3>Entrada para la actividad: ".$info['actividad_nombre']." </h3>";
            $message .= "<p>Hola ". $info['nombre'].", tus entradas para ".$info['actividad_nombre'].", el día de  ".$info['funcion_dia']." a las ".$info['funcion_hora']." horas, fueron adquiridas correctamente.</p>";
            $message .= "<p> Se enviarán las instrucciones para ingresar a la Actividad en un próximo email </p>" ;
            $message .= "<p> Ante cualquier duda puedes comunicarte al email contacto@espaciodiana.cl </p>" ;
        
            $message .= '</div>';
            $message .= '</div>';
        }
    
        $message .='</body>';
        $message .='</html>';

    }

    mail($to,$subject,$message, $headers);

    $confirmacion = array(
        "confirm_state" => "correct",
        "confirm_message" => "Email enviado correctamente",   
    );

    return $confirmacion;
}

// ===========================================ACTIVIDADES
function recalcularDatosDeTodasActividades(){

    // Recuperar listado de actividades   
    include 'connection.php';
    $actividades = $conn->query("SELECT * FROM actividades");

    // Por cada actividad recuperar listado de funciones
    foreach($actividades as $actividad){
        $actividad_id = $actividad['actividad_id'];

        //Reescribir en cada actividad la informacion
        recalcularDatosParaActividad($actividad_id);
    }
}

function recalcularDatosParaActividad($actividad_id){

    $entradas_count = 0;
    $monto_recolectado = 0;

    // Recuperar listado de funciones
    include 'connection.php';
    $funciones = $conn->query("SELECT * FROM funciones WHERE funcion_actividad_id = '$actividad_id'");

    // Por cada funcion recalcular su información
    foreach($funciones as $funcion){
        $funcion_id = $funcion['funcion_id'];
        $entradas_count  = $entradas_count + recalcularEntradasAceptadasParaFuncion($funcion_id);
        $monto_recolectado = $monto_recolectado + recalcularMontoRecolectadoParaFuncion($funcion_id);
    }

    //Reescribir en cada actividad la informacion
    include 'connection.php';
    $stmt = $conn->query("UPDATE actividades SET actividad_entradas_aceptadas = $entradas_count, actividad_monto_recolectado = $monto_recolectado WHERE actividad_id = '$actividad_id'");
    $conn->close();

    $response = array(
        'state' => 'correct',
        'text' => 'Actividad recalculada'
        );

    echo json_encode($response);
}

// ===========================================ENTRADAS

function crearEntrada($entrada_info){
    $code = strval(rand(100000, 999999));

    $nombre = $entrada_info['nombre'];
    $email = $entrada_info['email'];
    $funcion_id = $entrada_info['funcion_id'];

    $nombre_nino = "---"; if(array_key_exists('nombre_nino', $entrada_info)){ $nombre_nino = $entrada_info['nombre_nino']; }
    $telefono= "000"; if(array_key_exists('telefono', $entrada_info)){ $telefono = $entrada_info['telefono']; }

    // Crear entrada
    include 'connection.php';
    $conn->query("INSERT INTO entradas (
        entrada_nombre, entrada_email, entrada_funcion_id, orden_token, entrada_code, orden_aceptada, taller_nino, taller_fono)
        VALUES (
        '$nombre','$email','$funcion_id', 'entrada', '$code' , 1, '$nombre_nino', '$telefono' 
        )"
        );

    $entrada_id = $conn->insert_id;
        
    $conn->close();

    //Reducir cantidad de entradas disponibles
    include 'connection.php';

    $stmt = $conn->query("SELECT funcion_entradas_disponibles FROM funciones
        WHERE funcion_id = $funcion_id");

    $result = $stmt->fetch_assoc();
    $entradas_disponibles = $result['funcion_entradas_disponibles'];

    $entradas_restantes = $entradas_disponibles - 1;

    $conn->query("UPDATE funciones SET 
        funcion_entradas_disponibles = '$entradas_restantes'
        WHERE funcion_id = '$funcion_id'");
    $conn->close();

    $return = array(
        "entrada_id" => $entrada_id,
        "entrada_code" => $code,
        "nombre_nino" => $nombre_nino,
        "telefono" => $telefono
    );

    return $return;
}

// ===========================================FUNCIONES
function recalcularEntradasAceptadasParaFuncion($funcion_id){

    $entradas_count = 0;

    // encontrar todas las entradas aceptadas correspondiente a esa funcion
    include 'connection.php';
    $entradas = $conn->query("SELECT * FROM entradas WHERE orden_aceptada = '1' AND entrada_funcion_id = '$funcion_id'");

    // Sumar las entradas aceptadas correspondietes
    foreach($entradas as $entrada){
        $entradas_count++;
    }
    
    // Reescribir la cantidad de entradas aceptadas
    $stmt = $conn->query("UPDATE funciones SET funcion_entradas_aceptadas = $entradas_count WHERE funcion_id = '$funcion_id'");
    $conn->close();

    return $entradas_count;
}

function recalcularMontoRecolectadoParaFuncion($funcion_id){

    $monto_recolectado = 0;

    // encontrar todas las entradas aceptadas correspondiente a esa funcion
    include 'connection.php';
    $entradas = $conn->query("SELECT * FROM entradas WHERE orden_aceptada = '1' AND entrada_funcion_id = '$funcion_id'");

    // Sumar la cantidad recolectada por las entradas aceptadas
    foreach($entradas as $entrada){
        $monto_recolectado = $monto_recolectado + $entrada['entrada_pagado'];
    }

    // Reescribir la cantidad recolectada de entradas aceptadas
    $stmt = $conn->query("UPDATE funciones SET funcion_monto_recolectado = $monto_recolectado WHERE funcion_id = '$funcion_id'");
    $conn->close();

    return $monto_recolectado;
}


