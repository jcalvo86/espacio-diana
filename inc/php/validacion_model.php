<?php


$action = $_POST['action'];

if ($action === 'validar_entrada') {
    $user_codigo = $_POST['user_codigo'];
    $funcion_id = $_POST['funcion_id'];
    include 'connection.php';
    try {
        //Get 
        $stmt = $conn->query("SELECT * FROM entradas WHERE orden_aceptada = '1' AND entrada_funcion_id = '$funcion_id'");

        $stmt = $stmt->fetch_assoc();

        $db_entrada_code = $stmt['entrada_code'];
        $db_entrada_id = $stmt['entrada_id'];
        $db_entrada_utilizando = $stmt['entrada_utilizando'];

        //confirmar si existe y si el pass es correcto
        if ($db_entrada_code) {
            if ($db_entrada_code == $user_codigo) {
                if($db_entrada_utilizando == 0){

                //Bloquear codigo temporalmente
                // $stmt = $conn->query("UPDATE entradas SET entrada_utilizando = 1 WHERE entrada_id = '$db_entrada_id'");
                
                //Desbloquar codigo con evento
                // $nombre_event= "clear".$db_entrada_id;

                // $stmt = $conn->query("CREATE EVENT $nombre_event
                // ON SCHEDULE AT CURRENT_TIMESTAMP + INTERVAL 1 MINUTE
                // DO UPDATE entradas SET entrada_utilizando = 0 WHERE entrada_id = '$db_entrada_id'");

                $response = array(
                    'state' => 'correct',
                    'code' => $db_entrada_code,
                    'entrada_id' => $db_entrada_id
                   );
                }else{
                    $response = array(
                        'state' => 'error',
                        'text' => 'Este código está siendo utilizado.'
                       );
                }
            } else {
                $response = array(
              'state' => 'error',
              'text' => 'No coincide el código ingresado'
             );
            }
        } else {
            $response = array(
            'state' => 'error',
            'text' => 'No existe usuario con ese nombre'
           );
        }
        $conn->close();
    } catch (Exception $e) {
        //en caso de un error, tomar la exepcion
        $response = array(
      'state' => 'error',
      'pass'=> $e->getMesage()
     );
    }

    echo json_encode($response);
}

if ($action === 'revalidar_entrada') {

    $entrada_id = $_POST['entrada_id'];

    include 'connection.php';
    $stmt = $conn->query("SELECT * FROM entradas WHERE entrada_id = '$entrada_id'");
    $stmt = $stmt->fetch_assoc();
    $db_entrada_utilizando = $stmt['entrada_utilizando'];
    if($db_entrada_utilizando == 0){
        $stmt = $conn->query("UPDATE entradas SET entrada_utilizando = 1 WHERE entrada_id = '$entrada_id'");
        $nombre_event= "clear".$entrada_id;
        $stmt = $conn->query("CREATE EVENT $nombre_event
        ON SCHEDULE AT CURRENT_TIMESTAMP + INTERVAL 1 MINUTE
        DO UPDATE entradas SET entrada_utilizando = 0 WHERE entrada_id = '$entrada_id'");
    }
}