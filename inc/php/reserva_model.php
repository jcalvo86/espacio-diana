<?php
include 'actividades_list.php';

$action = $_POST['action'];

if ($action === 'realizar_reserva') {
    $user_email = $_POST['user_email'];
    $entrada_cant = $_POST['entrada_cant'];
    $user_nombre0 = $_POST['user_nombre0'];
    $user_nombre1 = $_POST['user_nombre1'];
    $user_nombre2 = $_POST['user_nombre2'];
    $user_nombre3 = $_POST['user_nombre3'];
    $user_nombre4 = $_POST['user_nombre4'];

    $nombre_nino = $_POST['nombre_nino'];
    $telefono = $_POST['telefono'];
    $funcion_id = $_POST['funcion_id'];

    $code = strval(rand(100000, 999999));

        include 'connection.php';
        $conn->query("INSERT INTO entradas (
            entrada_nombre, entrada_email, entrada_funcion_id, orden_token, entrada_code, orden_aceptada, taller_nino, taller_fono)
            VALUES (
            '$user_nombre0','$user_email','$funcion_id', 'reserva', $code , 1, '$nombre_nino', '$telefono' 
            )"
          );
          
          $conn->close();

          $entradas = array();
          array_push($entradas, $user_nombre0);


        if($entrada_cant >= 2){
            include 'connection.php';
            $conn->query("INSERT INTO entradas (
                entrada_nombre, entrada_email, entrada_funcion_id, orden_token, entrada_code, orden_aceptada)
                VALUES (
                '$user_nombre1','$user_email','$funcion_id', 'reserva', $code , 1
                )"
              );
              
              $conn->close();

              array_push($entradas, $user_nombre1);

        }

        if($entrada_cant >= 3){
            include 'connection.php';
            $conn->query("INSERT INTO entradas (
                entrada_nombre, entrada_email, entrada_funcion_id, orden_token, entrada_code, orden_aceptada)
                VALUES (
                '$user_nombre2','$user_email','$funcion_id', 'reserva', $code , 1
                )"
              );
              
              $conn->close();

              array_push($entradas, $user_nombre2);

        }

        if($entrada_cant >= 4){
            include 'connection.php';
            $conn->query("INSERT INTO entradas (
                entrada_nombre, entrada_email, entrada_funcion_id, orden_token, entrada_code, orden_aceptada)
                VALUES (
                '$user_nombre3','$user_email','$funcion_id', 'reserva', $code , 1
                )"
              );
              
              $conn->close();

              array_push($entradas, $user_nombre3);

        }

        if($entrada_cant >= 5){
            include 'connection.php';
            $conn->query("INSERT INTO entradas (
                entrada_nombre, entrada_email, entrada_funcion_id, orden_token, entrada_code, orden_aceptada)
                VALUES (
                '$user_nombre4','$user_email','$funcion_id', 'reserva', $code , 1
                )"
              );
              
              $conn->close();

              array_push($entradas, $user_nombre4);

        }



        //Reducir cantidad de entradas disponibles
        include 'connection.php';

        $stmt = $conn->query("SELECT funcion_entradas_disponibles FROM funciones
        WHERE funcion_id = $funcion_id");

        $result = $stmt->fetch_assoc();
        $entradas_disponibles = $result['funcion_entradas_disponibles'];

        $entradas_restantes = $entradas_disponibles - $entrada_cant;

        $conn->query("UPDATE funciones SET 
        funcion_entradas_disponibles = '$entradas_restantes'
        WHERE funcion_id = '$funcion_id'");
        $conn->close();


 // EMAIL SEND

        $funcion = getFuncionInfo($funcion_id);

        $from = "boleteria@espaciodiana.cl";
        $to = $user_email;
        $subject = 'entradas '.$funcion['actividad_nombre'];

        $headers = "De:" . $from;
        $headers .= "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "Return-Path: Espacio Diana <contacto@espaciodiana.cl>\r\n"; 
        $headers .= "Organization: Espacio Diana\r\n";
        $headers .= 'From: Boleteria Espacio Diana <boleteria@espaciodiana.cl>' . "\r\n";
        
        $message ='';

        $message .='<!DOCTYPE html>';
        $message .='<html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">';
        $message .='<head>';

        $message .='<style>';
        $message .='p {color:white !important; }';
        $message .='.mail{display:flex; flex-flow:row; }';
        $message .='.image{background-size:cover; background-position:center; width:40%;}';
        $message .='.content{width:100%; display:block; min-height:400px; font-size:16px; background-color:black; color:white; padding:10px;}';
        $message .='a {text-decoration:none; font-weight:bold; color:black; background-color:white; border-radius:10px; padding:5px;}'; 
        $message .='tr{ border-style: solid; border-color: white; border-width: 1px;}';
        $message .='table{ border-style: solid; border-color: white; border-width: 1px; width: 50%; text-align: center; margin-bottom:20px;}';

        $message .='</style>';
        $message .='</head>';

        $message .='<body>';
    
        if($funcion['actividad_tipo'] == "obra"){

            $message .='<div class="mail">';
            $message .='<div class="image" style="background-image:url(https://www.espaciodiana.cl/files/actividad/'.$funcion['actividad_id'].'/portada.jpg); "></div>';
            $message .='<div class="content">';
        
            $message .= "<h3>Entrada para ".$funcion['actividad_nombre']." </h3>";
            $message .= "<p>Hola ". $user_nombre0.", tus entradas para ".$funcion['actividad_nombre']." en su función de  ".$funcion['funcion_dia']." / ".$funcion['funcion_hora'].", fueron adquiridas correctamente.</p>";
        
            $message .= '<table class="table">';
            $message .= '<thead><tr><th scope="col">Nombre</th><th scope="col">Código</th></tr></thead><tbody>';
        
            foreach($entradas as $entrada){
                $message .= '<tr><td>'.$entrada['entrada_nombre'].'</td><td>'.$entrada['entrada_code'].'</td></tr>';
            }
            $message .= '</tbody></table>';
        
            $message .= "<a href='www.espaciodiana.cl/funcion.php?funcion_id=0' class='btn btn-rojo btn-fullwidth'>Ver Obra</a>" ;
            
            $message .= "<p> La obra quedará habilitada para que puedas presenciarla desde ".$funcion['funcion_habilitado']." a ".$funcion['funcion_fin']." horas del día de la función </p>" ;
            $message .= "<p> Ante cualquier duda puedes comunicarte al email contacto@espaciodiana.cl </p>" ;
        
            $message .= '</div>';
            $message .= '</div>';
    
        }else if($funcion['actividad_tipo'] == "taller"){
    
            $message .='<div class="mail">';
            $message .='<div class="image" style="background-image:url(https://www.espaciodiana.cl/files/actividad/'.$funcion['actividad_id'].'/portada.jpg); "></div>';
            $message .='<div class="content">';
        
            $message .= "<h3>Entrada para ".$funcion['actividad_nombre']." </h3>";
            $message .= "<p>".$user_nombre0.", tus entradas para ".$funcion['actividad_nombre']." están listas.";
            $message .= "<p>Te enviaremos las instrucciones para ingresar al taller proximamente.</p>" ;
            $message .= "<p>Recuerda que estas entradas son para el día de  ".$funcion['funcion_dia']." a las ".$funcion['funcion_hora']." horas.</p>";
            $message .= "<p>Ante cualquier duda puedes comunicarte al email contacto@espaciodiana.cl </p>" ;

            if($funcion['actividad_is_infantil']==1){
            $message .= '<table class="table">';
            $message .= '<thead><tr><th scope="col">Asistente</th></tr></thead><tbody>';

            $message .= '<tr><td>'.$nombre_nino.'</td></tr>';

            $message .= '</tbody></table>';
            }

            $message .= '</div>';
            $message .= '</div>';
    
        }else{
    
            $message .='<div class="mail">';
            $message .='<div class="image" style="background-image:url(https://www.espaciodiana.cl/files/actividad/'.$funcion['actividad_id'].'/portada.jpg); "></div>';
            $message .='<div class="content">';
        
            $message .= "<h3>Entrada para la actividad: ".$funcion['actividad_nombre']." </h3>";
            $message .= "<p>Hola ". $user_nombre0.", tus entradas para ".$funcion['actividad_nombre'].", el día de  ".$funcion['funcion_dia']." a las ".$funcion['funcion_hora']." horas, fueron adquiridas correctamente.</p>";
            $message .= "<p> Se enviarán las instrucciones para ingresar al taller en un próximo email </p>" ;
            $message .= "<p> Ante cualquier duda puedes comunicarte al email contacto@espaciodiana.cl </p>" ;
        
            $message .= '</div>';
            $message .= '</div>';
        }

        $message .='</body>';
        $message .='</html>';

        mail($to,$subject,$message, $headers);

        $response = array(
            'state' => 'correct',
            'text' => 'Entradas reservadas',
            'code' => $code,
            'funcion' => $funcion_id
           );

    echo json_encode($response);
}