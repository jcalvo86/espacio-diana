<?php
include 'actividades_list.php';
include 'funciones.php';

$action = $_POST['action'];

if ($action === 'generarEntradaNueva') {

    $entrada = crearEntrada($_POST);

    // EMAIL SEND

    $funcion = getFuncionInfo($_POST['funcion_id']);

    $email_info = array(
        "email_action" => "enviar_entrada",
        "from" => 'boleteria@espaciodiana.cl',
        "email" => $_POST['email'],
        "nombre" => $_POST['nombre'],
        "entrada_id" => $entrada['entrada_id'],
        "entrada_code" => $entrada['entrada_code'],
        "actividad_id" => $funcion['actividad_id'],
        "actividad_nombre" => $funcion['actividad_nombre'],
        "actividad_tipo" => $funcion['actividad_tipo'],
        "funcion_id" => $funcion['funcion_id'],
        "funcion_dia" => $funcion['funcion_dia'],
        "funcion_hora" => $funcion['funcion_hora'],
        "funcion_habilitado" => $funcion['funcion_habilitado'],
        "funcion_fin" => $funcion['funcion_fin'],
    );   

    $confirmacion = enviarEmail($email_info);

    $response = array(
        'state' => $confirmacion['confirm_state'],
        'text' => $confirmacion['confirm_message'],
        'code' => $entrada['entrada_code'],
        'funcion' => $funcion['funcion_id']
        );

    echo json_encode($response);
}


if ($action === 'reenviarEmailEntrada') {

    if(array_key_exists('entrada_id', $_POST)){ 
        $entrada_id = $_POST['entrada_id']; 
        $entrada = getEntradaInfo($entrada_id);

        $email_info = array(
            "email_action" => "reenviar_entrada",
            "from" => 'boleteria@espaciodiana.cl',
            "email" => $entrada['entrada_email'],
            "nombre" => $entrada['entrada_nombre'],
            "entrada_id" => $entrada['entrada_id'],
            "entrada_code" => $entrada['entrada_code'],
            "actividad_id" => $entrada['actividad_id'],
            "actividad_nombre" => $entrada['actividad_nombre'],
            "actividad_tipo" => $entrada['actividad_tipo'],
            "funcion_id" => $entrada['funcion_id'],
            "funcion_dia" => $entrada['funcion_dia'],
            "funcion_hora" => $entrada['funcion_hora'],
            "funcion_habilitado" => $entrada['funcion_habilitado'],
            "funcion_fin" => $entrada['funcion_fin'],
            "taller_fono" => $entrada['taller_fono'],
            "taller_nino" => $entrada['taller_nino']
        );   

        $confirmacion = enviarEmail($email_info);

        $response = array(
            'state' => $confirmacion['confirm_state'],
            'text' => $confirmacion['confirm_message'],
           );
    
    }else{ 
        $response = array(
            'state' => "error",
            'text' => "No existe esta entrada en la base de datos.",
           );;
    }

    echo json_encode($response);
}