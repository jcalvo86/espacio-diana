<?php

    include 'connection.php';
    date_default_timezone_set('America/Santiago');

    $today = getdate();
    $upload_date = $today['year'] . "-" . $today['mon'] . "-" . $today['mday'];
    $upload_hour = $today['hours'] .  $today['minutes'] . $today['seconds'];
    $upload_datetime = $today['year'] . "-" . $today['mon'] . "-" . $today['mday']."T".$today['hours'] .":".  $today['minutes'] .":". $today['seconds'];
    $action = $_POST['action'];

  //**__________________PLANILLAS DE SINIESTRO_______________________ */
  if ($action == "pregunta_upload"){

    //-----Informacion 
      $pregunta_respuesta = $_POST['pregunta_respuesta'];
      $pregunta_usuario = $_POST['pregunta_usuario'];
      
      $stmt = $conn->prepare("INSERT INTO pregunta_hoy (
        preg_texto, preg_nombre
        )
        VALUES (
        '$pregunta_respuesta', '$pregunta_usuario'
        )"
      );

      $stmt->execute();
      $inserted_id = $stmt->insert_id;
      $stmt->close();

      $response = array(
        'state' => 'correct',
        'text' => 'Mensaje enviado',
        'inserted_id' => $inserted_id,
        'nombre' => $pregunta_usuario
        );

      die(json_encode($response));
  }

  if ($action == "updateEmail"){

    //-----Informacion 
      $id_usuario = $_POST['id_usuario'];
      $email_usuario = $_POST['email_usuario'];
      
      $stmt = $conn->prepare("UPDATE pregunta_hoy SET 
        preg_email = '$email_usuario'
        WHERE preg_id = '$id_usuario'"
      );

      $stmt->execute();
      $stmt->close();

      $response = array(
        'state' => 'correct',
        'text' => 'Email actualziado',
        );

      die(json_encode($response));
  }