<?php

$user_email = $_POST['user_email'];
$user_password = $_POST['user_password'];
$action = $_POST['action'];

if ($action === 'login') {

    include 'connection.php';
    try {

        //Get 
        $stmt = $conn->prepare("SELECT usuario_id, usuario_email, usuario_nombre, usuario_password FROM usuarios WHERE usuario_email = '$user_email'");
        $stmt->execute();
        $stmt->bind_result($db_usuario_id, $db_usuario_email, $db_usuario_nombre, $db_usuario_password);
        $stmt->fetch();

        //confirmar si existe y si el pass es correcto
        if ($db_usuario_email) {
            if (password_verify($user_password, $db_usuario_password)) {
                //INICIAR SESION
                session_start();
                $_SESSION['usuario_id'] = $db_usuario_id;
                $_SESSION['usuario_email'] = $db_usuario_email;
                $_SESSION['usuario_nombre'] =  $db_usuario_nombre;
                $_SESSION['login'] = true;

                $response = array(
                    'state' => 'correct',
                    'name' => $db_usuario_nombre
                   );

            } else {
                $response = array(
              'state' => 'error',
              'text' => 'No coincide el usuario con el password'
             );
            }
        } else {
            $response = array(
            'action' => $action,
            'state' => 'error',
            'text' => 'No existe usuario con ese nombre'
           );
        }
        $stmt->close();
        $conn->close();
    } catch (Exception $e) {
        //en caso de un error, tomar la exepcion
        $response = array(
      'state' => 'error',
      'pass'=> $e->getMesage()
     );
    }
}

 echo json_encode($response);
