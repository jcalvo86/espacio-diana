<script type="text/javascript">
document.cookie = 'window_width=' + window.innerWidth;
</script>

<?php
  include 'inc/template/navbar_landing.php';
  include 'inc/php/pregunta_list.php';

  $preguntas = get_preguntas_list();
  $preguntas_fondo = get_preguntas_list_parafondo();
  $cantidad = get_preguntas_amount();

  $width = $_COOKIE['window_width'];

  $calls = 9;

  if($width <= '1000'){
    $calls = 6;
  }
?>

<html>

<head>
    <link href="inc/css/calendar.css" type="text/css" rel="stylesheet" />
</head>

<body>
    <div class="contenedor">

        <div class="segment_pregunta">
            <div class="graffiti_fondo" style="height:40%;">
                <?php

            for( $i = 0; $i < $calls; $i++){
              $margintop = ($i * rand ( 5 , 15 ) )+rand ( -20 , 20 );
              $marginleft = ($i * rand ( 0 , 15 ))+rand ( -20 , 20 );
              ?>
                <h4 class="item" id="word<?php echo $i; ?>" style="margin-left:<?php echo $marginleft;?>px; margin-top:<?php echo $margintop;?>px;"><?php echo $preguntas_fondo[$i];?></h4>

                <?php }?>
            </div>

            <div class="pregunta_container" style="min-height:400px; justify-content:center;">
                <form id="pregunta_form" action="pregunta" style="width: 100%;">
                  <h3 style="text-align: center;">¿Qué te gustaría que pasara aquí?</h3>
                  <?php 
                  $placeholder_options = array('Quiero un unicornio de muchos colores' , 'Quiero tocar en una banda de rock', 'Quiero ser el mejor gamer de todos los tiempos');
                  $placeholder = $placeholder_options[rand(0,2)];?>
                    <div class="input-group">
                        <textarea id="pregunta_respuesta" class="form-control" aria-label="With textarea"
                            placeholder="<?php echo $placeholder;?>" data-container="body"
                            data-toggle="popover" data-placement="bottom"
                            data-content="¿Seguro que no te interesa que pasen cosas en este lugar?"></textarea>
                    </div>


                    <div class="form-group">
                        <input type="text" class="form-control" id="pregunta_usuario" aria-describedby="emailHelp"
                            placeholder="Hola, me llamo..." data-container="body" data-toggle="popover"
                            data-placement="bottom" data-content="Mi nombre es Diana, ¿Y el tuyo?.">
                        <small class="form-text text-muted">No compartiremos tu información.</small>
                    </div>

                    <button type="submit" class="btn btn-primary btn-lg">Enviar</button>
                </form>
            </div>
        </div>

    </div>
</body>

</html>


<div id="modalNewsletter" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Gracias <span id="nombre_usuario"></span> </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3>Estamos buscando dar vida a este espacio, con todas nuestras ideas.</h3>
                <p>Si quieres que te informemos de nuestras novedades, puedes inscribirte en nuestro newsletter.</p>
                <form id="updateEmail_form">
                    <div class="form-group">
                        <input type="text" id="id_usuario" hidden>
                        <input type="email" class="form-control" id="email_usuario" aria-describedby="emailHelp"
                            placeholder="Ingresa tu email">
                        <small id="emailHelp" class="form-text text-muted">No compartiremos tu información.</small>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Quiero estar al tanto</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="modalSuccess" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h3>Tu email ha sido guardado</h3>
                <p>Pronto recibirás más noticias de lo que está sucediendo en Diana.</p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
    crossorigin="anonymous"></script>
<script src="inc/js/parallax.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
    integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
</script>

<script src="inc/js/listener.js"></script>

<script>
$('.carousel').carousel({
    interval: 6000
})


$(function() {
    count = 0;
    wordsArray = <?php echo json_encode($preguntas_fondo);?>;

    duration = 6000;
    interval = 2000;

    setInterval(function() {
        count++;
        $("#word0").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word1").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word2").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word3").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word4").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word5").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word6").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word7").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word8").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word9").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word10").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));

    setInterval(function() {
        count++;
        $("#word11").fadeOut(interval, function() {
            $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(
                interval);
        });
    }, Math.floor(Math.random() * interval + duration));
});
</script>