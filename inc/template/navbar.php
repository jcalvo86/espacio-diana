<?php

  if (isset($_GET['end_session'])) {
      $_SESSION = array();
  }
  
  include 'inc/php/funciones.php';
  include 'inc/php/connection.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MKLNHFR');</script>
<!-- End Google Tag Manager -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    
    <link rel="stylesheet" href="inc/css/style.css" type="text/css"  >
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" > 
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;500;700;900&family=Roboto:wght@300;400;700&display=swap" >
    <link rel="stylesheet" href="inc/css/calendar.css" type="text/css" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Espacio Diana</title>
    
</head>
<body>

<div class="body__container">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MKLNHFR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<?php
$actualPage = getPage();

if ($actualPage === 'index' ) {
    echo '<script src="inc/js/login.js"></script>';
?>

<nav class="navbar navbar-expand-lg">
  <div class="container">
    <a class="navbar-brand" href="index_main.php"><img src="files/logodiana_isotipo_negro.png" alt="" style="width:60px"></a>
  </div>
</nav>

<?php
}else{
?>


<nav class="navbar navbar-expand-lg">

<!-- NAVBAR SMALL -->
<div class="navbar--small">
  <div class="navbar__container">
    <a class="navbar-brand" href="index_main.php"><img src="files/logodiana_isotipo_negro.png" alt="" style="width:60px"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"><i class="fas fa-bars" style="color:white;"></i></span>
    </button>
  </div>


  <div class="collapse navbar-collapse navbar_navbar" id="navbarTogglerDemo01">
    <div class="" style="justify-content: center; align-items: center;">
        <a href="https://www.facebook.com/espaciodiana/" style="color:white;"><i class="fab fa-facebook-f fa-1x" style="margin-left:10px;"></i></a>
        <a href="https://www.instagram.com/espaciodiana/" style="color:white;"><i class="fab fa-instagram fa-1x" style="margin-left:10px;"></i></a>
        <a href="https://twitter.com/espacio_diana" style="color:white;"><i class="fab fa-twitter fa-1x" style="margin-left:10px;"></i></a>
    </div>

    <div class="div" style="margin-bottom: 10px; text-align: center;">
      <a class="nav-link" href="nosotros.php">Quienes Somos</a>

      <a class="nav-link" href="cartelera.php">Cartelera</a>

      <a class="nav-link" href="servicios.php">Servicios</a>

      <a class="nav-link" href="contacto.php">Contacto</a>
    </div>


  </div>
</div>

  <!-- NAVBAR NORMAL -->
<div class="navbar--large">
  <div class="navbar_navbar">
    <div class="flex-row" style="justify-content: center; align-items: center;">
      <a class="navbar-brand" href="index_main.php"><img src="files/logodiana_isotipo_negro.png" alt="" style="width:60px"></a>
        <a href="https://www.facebook.com/espaciodiana/" style="color:white;"><i class="fab fa-facebook-f fa-1x" style="margin-left:10px;"></i></a>
        <a href="https://www.instagram.com/espaciodiana/" style="color:white;"><i class="fab fa-instagram fa-1x" style="margin-left:10px;"></i></a>
        <a href="https://twitter.com/espacio_diana" style="color:white;"><i class="fab fa-twitter fa-1x" style="margin-left:10px;"></i></a>
    </div>

    
    <ul class="navbar-nav mt-2 mt-lg-0">

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="nosotros.php">Quienes Somos</a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="nosotros.php#historia">Historia</a>
          <a class="dropdown-item" href="nosotros.php#vision">Visión</a>
          <a class="dropdown-item" href="nosotros.php#mision">Misión</a>
          <!-- <div class="dropdown-divider"></div> -->
          <a class="dropdown-item" href="nosotros.php#equipo">Equipo</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="cartelera.php" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cartelera</a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="cartelera.php#teatro">Teatro</a>
          <!-- <a class="dropdown-item" href="cartelera.php/#vision">Danza</a> -->
          <a class="dropdown-item" href="cartelera.php/#taller">Talleres</a>
           <!-- <a class="dropdown-item" href="cartelera.php/#mision">Concierto</a> -->
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="cartelera.php#familiar">Familiar</a>
          <a class="dropdown-item" href="cartelera.php">Cartelera</a>
        </div>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="servicios.php">Servicios</a>
      </li>

      <!-- <li class="nav-item">
        <a class="nav-link" href="noticias.php">Noticias</a>
      </li> -->

      <li class="nav-item">
        <a class="nav-link" href="contacto.php">Contacto</a>
      </li>
    </ul>
  </div>
</div>




</nav>


<?php
}
?>

