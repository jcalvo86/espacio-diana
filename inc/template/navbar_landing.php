<?php

  if (isset($_GET['end_session'])) {
      $_SESSION = array();
  }
  
  // include 'inc/php/funciones.php';
  // include 'inc/php/connection.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    
    <link type="text/css" rel="stylesheet" href="inc/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet"> 
    <link href="inc/css/calendar.css" type="text/css" rel="stylesheet" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Espacio Diana</title>
    
</head>
<body>
