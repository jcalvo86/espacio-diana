<?php

  if (isset($_GET['end_session'])) {
      $_SESSION = array();
  }
  
  include '../inc/php/funciones.php';
  include '../inc/php/connection.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    
    <link rel="stylesheet" href="../inc/css/style.css" type="text/css"  >
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" > 
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;500;700;900&family=Roboto:wght@300;400;700&display=swap" rel="stylesheet">
    <link href="../inc/css/calendar.css" type="text/css" rel="stylesheet" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Espacio Diana</title>
    
</head>
<body>


<nav class="navbar navbar-expand-lg">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"><i class="fas fa-bars" style="color:white;"></i></span>
  </button>

  <div class="collapse navbar-collapse navbar_navbar" id="navbarTogglerDemo01">
    <div class="flex-row" style="justify-content: center; align-items: center;">
      <a class="navbar-brand" href="../index_main.php"><img src="../files/logodiana_isotipo_negro.png" alt="" style="width:60px"></a>
        <i class="fab fa-facebook-f fa-3x" style="margin-left:10px;"></i>
        <i class="fab fa-instagram fa-3x" style="margin-left:10px;"></i>
        <i class="fab fa-twitter fa-3x" style="margin-left:10px;"></i>
    </div>

    
    <ul class="navbar-nav mt-2 mt-lg-0">

      <li class="nav-item">
        <a class="nav-link" href="../index_main.php">Volver</a>
      </li>
    </ul>
  </div>
</nav>


<div class="body__container flex-row">

  <div class="nav_lateral">
    <div class="nav_lateral__profile">
      
    </div>

    <a href="actividades.php" class="nav_lateral__item">
      Actividades y Obras
    </a>
    
    <a href="entradas.php" class="nav_lateral__item">
      Entradas
    </a>

    <a href="invitaciones.php" class="nav_lateral__item">
      Invitaciones
    </a>

    <a href="noticias.php" class="nav_lateral__item">
      Noticias
    </a>

  </div>
  <div class="nav_lateral__contentarea">
  

