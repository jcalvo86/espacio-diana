</div><!--nav_lateral__contentarea END-->
</div><!--body__container END-->

<div class="footer flex-row layout-box" style="justify-content: space-between;">
    <div class="flex-col  flex-1">
        <img src="../files/logodiana_web_negro.png" alt="" style="width:200px; margin-bottom:2rem;">
    </div>

    <div class="flex-col flex-1">
        <h4>Suscribete para m谩s informaci贸n</h4>
        <form action="subscribe">
            <div class="form-group">
                <label for="exampleInputEmail1" style="font-size:1.6rem;">Email</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <small id="emailHelp" class="form-text text-muted"  style="font-size:1.6rem;">No compartiremos tu informaci贸n.</small>
            </div>
            <button type="submit" class="btn btn-primary">Suscribirse</button>
        </form>
    </div>
</div>


        <!-- TOAST -->
        <div class="toast" style="position: fixed; top: 60px; right: 20px;">
            <div class="toast-header">
            <strong class="mr-auto" id="toast-title">Bootstrap</strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="toast-body" id="toast-message">
            Hello, world! This is a toast message.
            </div>
        </div>

</body></html>

<script src="../inc/js/functions.js"></script>

<?php
$actualPage = getPage();

if($actualPage === 'entradas'){
    echo '<script src="../inc/js/listener.js"></script>';
 }
 ?>

<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
    crossorigin="anonymous"></script>
<script src="../inc/js/parallax.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
    integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
</script>

<script>
$('.dropdown-toggle').dropdown();
$('.toast').toast({delay:2000})





</script>