function pregunta_upload(e) {

    e.preventDefault();
    console.log("CORRIENDO");


    var data = new FormData();

    console.log("worksheet upload start");
    var action = "uploadWorksheet";
    var worksheet_user = document.getElementById("worksheet_user");

    var shipping_company = document.getElementById("shipping_company");

    console.log("shipipng id " + worksheet_user.value);
    if (worksheet_user.value == "") {
        var toastHTML = "Debe ingresar el agente correspondiente a la planilla que va a cargar.";
        toastr.options = { "timeOut": "10000" };
        toastr.error(toastHTML);
        worksheet_user.focus();
    } else {
        var insurance_company = document.getElementById("insurance_company");
        console.log("insurance company: " + insurance_company.value);
        if (insurance_company.value == "") {
            insurance_company.focus();
            var toastHTML = "Debe ingresar la aseguradora correspondiente a la planilla que va a cargar.";
            toastr.error(toastHTML);
        } else {
            var worksheet_type = document.getElementById("worksheet_type");
            if (worksheet_type.value == "") {
                worksheet_type.focus();
                var toastHTML = "Debe ingresar si la planilla corresponde a Daño o Demora.";
                toastr.error(toastHTML);
            } else {
                var worksheet_date = document.getElementById("worksheet_date");
                if (worksheet_date.value == "") {
                    worksheet_date.focus();
                    var toastHTML = "Debe ingresar la fecha correspondiente a la planilla.";
                    toastr.error(toastHTML);
                } else {
                    var worksheetFile = document.getElementById("worksheet");
                    if (worksheetFile.files[0] == undefined) {
                        var toastHTML = 'Debe adjuntar una planilla.';
                        toastr.error(toastHTML);
                        worksheetFile.focus();
                    } else {
                        toastr.info('Iniciando proceso de carga de planilla. <br>Espere hasta que su planilla haya terminado de cargar.');
                        data.append("worksheet", worksheetFile.files[0]);
                        data.append("action", action);
                        data.append("worksheet_user", worksheet_user.value);
                        data.append("insurance_company", insurance_company.value);
                        data.append("worksheet_type", worksheet_type.value);
                        data.append("worksheet_date", worksheet_date.value);

                        var xhr = new XMLHttpRequest();
                        xhr.open("POST", "../inc/php/worksheet_model.php", true);
                        xhr.onload = function() {

                            if (this.status === 200) {
                                console.log("200");
                                console.log(xhr.responseText);
                                var response = JSON.parse(xhr.responseText);

                                console.log(response);
                                if (response.state !== "correct") {
                                    var toastHTML = response.text;
                                    toastr.error(toastHTML);
                                    return;
                                } else {
                                    var toastHTML = response.text;
                                    // var toastHTML = "Planilla ingresada correctamente";
                                    toastr.options.onHidden = function() { window.location.href = "worksheet.php"; };
                                    toastr.success(toastHTML);
                                    return;
                                }
                            }
                        };
                        xhr.send(data);
                    }
                }
            }
        }
    }
}