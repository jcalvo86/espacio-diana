<?php

include 'inc/php/actividades_list.php';

// print_r($post);

if (isset($_GET['funcion'])) {
    $funcion_id = $_GET['funcion'];
}

if (isset($_GET['code'])) {
    $code = $_GET['code'];
}

$funcion = getFuncionInfoPorFuncionIdAndCode($funcion_id, $code);

$contacto = $funcion->fetch_assoc();
// print_r($funcion);
// print_r($contacto);
// echo "///";

include 'inc/template/navbar.php';

?>

<style>
    body{
        font-size:16px;
    }
</style>

<div class="contenedor">
    <div class="segment_cabecera background-image" style="background-image: url('files/imagenes/img_0001_diana002.jpg'); max-height:200px; min-height:0px;">

        <div class="segment_cabecera_contenedor">
            <div class="segment_cabecera_titulo">
                <h1>Boleteria</h1>
            </div>
        </div>
    </div>
</div>

<!-- SI LA ACTIVIDAD ES UN TALLER -->
<?php if($contacto['actividad_tipo']=="taller"){?>

<div class="segment_boleteria flex-row layout-box">
    <div class='col-8'>
        <h2><?php echo $contacto['actividad_nombre']?></h2>
        <h4><?php echo $contacto['entrada_nombre']?> tus entradas para "<?php echo $contacto['actividad_nombre']?>" en su función del día <?php echo displayFecha($contacto['funcion_dia']);?> a las <?php echo $contacto['funcion_hora']?> horas, fueron adquiridas correctamente.</h4>
        <br>

        <div class="flex-row">
            <div class="col" style="padding-left:0;">
                <h4>Fecha:</h4>
                <p><?php echo displayFecha($contacto['funcion_dia']);?></p>
            </div>

            <div class="col">
                <h4>Hora:</h4>
                <p><?php echo $contacto['funcion_hora']?></p>
            </div>
        </div>

        <hr>

        <h4>Asistentes:</h4>
        <table class="table">
            <thead>
                <tr><th scope="col">Nombre</th></tr>
            </thead>
            <tbody>
            <?php
            foreach($funcion as $entrada){?>
                <tr><td><?php echo $entrada['taller_nino']?></td></tr>
            <?php } ?>
            </tbody>
        </table>

        <br>

        <p>Se ha envíado un email a <?php echo $contacto['entrada_email']?>, con las instrucciones para asistir a este taller.</p>

    </div>


    <div class="col-4 background-image" style="background-image:url(files/actividad/<?php echo $entrada['actividad_id'] ?>/portada.jpg); max-height: 40rem;">
        
    </div>

    <hr>
</div>

<div class="flex-col layout-box">
    <h4><?php echo $entrada['actividad_nombre']?></h4>
    <p><?php echo $entrada['actividad_descripcion']?></p>
</div>

<!-- SI LA ACTIVIDAD ES UNA OBRA -->
<?php }else if($contacto['actividad_tipo']=="obra"){?>

    <div class="segment_boleteria flex-row layout-box">
    <div class='col-8'>
        <h4><?php echo $contacto['entrada_nombre']?> tus entradas para "<?php echo $contacto['actividad_nombre']?>" en su función del día <?php echo displayFecha($contacto['funcion_dia']);?> a las <?php echo $contacto['funcion_hora']?> horas, fueron adquiridas correctamente.</h4>
        <br>

        <div class="flex-row">
            <div class="col" style="padding-left:0;">
                <h4>Fecha:</h4>
                <p><?php echo displayFecha($contacto['funcion_dia']);?></p>
            </div>

            <div class="col">
                <h4>Hora:</h4>
                <p><?php echo $contacto['funcion_hora']?></p>
            </div>
        </div>

        <div class="flex-row">
            <div class="col" style="padding-left:0;">
                <h4>Lugar:</h4>
                <p>Espacio Diana: Arturo Prat 435 Santiago</p>
            </div>
        </div>

        <h4>Asistentes:</h4>
        <table class="table">
            <thead>
                <tr><th scope="col">Nombre</th><th scope="col">Código</th></tr>
            </thead>
            <tbody>
            <?php
            foreach($funcion as $entrada){?>
                <tr><td><?php echo $entrada['entrada_nombre']?></td><td><?php echo $entrada['entrada_code']?></td></tr>
            <?php } ?>
            </tbody>
        </table>

        <br>

        <div class="flex-col flex-2">
            <h4><?php echo $entrada['actividad_nombre']?></h4>
            <p><?php echo $entrada['actividad_descripcion']?></p>
        </div>

        <div class="encuesta">
        </div>
    </div>


    <div class="col-4 background-image" style="background-image:url(files/actividad/<?php echo $entrada['actividad_id'] ?>/portada.jpg)" style="min-height:60rem;">
        <h2><?php echo $entrada['actividad_nombre']?></h2>
    </div>



    <hr>
</div>

<!-- SI LA ACTIVIDAD ES UNA ACTIVIDAD -->
<?php }else{?>

    <div class="segment_boleteria flex-row layout-box">
    <div class='col-8'>
        <h4><?php echo $contacto['entrada_nombre']?> tus entradas para "<?php echo $contacto['actividad_nombre']?>" en su función del día <?php echo displayFecha($contacto['funcion_dia']);?> a las <?php echo $contacto['funcion_hora']?> horas, fueron adquiridas correctamente.</h4>
        <br>

        <div class="flex-row">
            <div class="col" style="padding-left:0;">
                <h4>Fecha:</h4>
                <p><?php echo displayFecha($contacto['funcion_dia']);?></p>
            </div>

            <div class="col">
                <h4>Hora:</h4>
                <p><?php echo $contacto['funcion_hora']?></p>
            </div>
        </div>

        <div class="flex-row">
            <div class="col" style="padding-left:0;">
                <h4>Lugar:</h4>
                <p>Espacio Diana: Arturo Prat 435 Santiago</p>
            </div>
        </div>

        <h4>Asistentes:</h4>
        <table class="table">
            <thead>
                <tr><th scope="col">Nombre</th><th scope="col">Código</th></tr>
            </thead>
            <tbody>
            <?php
            foreach($funcion as $entrada){?>
                <tr><td><?php echo $entrada['entrada_nombre']?></td><td><?php echo $entrada['entrada_code']?></td></tr>
            <?php } ?>
            </tbody>
        </table>

        <br>

        <div class="flex-col flex-2">
            <h4><?php echo $entrada['actividad_nombre']?></h4>
            <p><?php echo $entrada['actividad_descripcion']?></p>
        </div>

        <div class="encuesta">
        </div>
    </div>


    <div class="col-4 background-image" style="background-image:url(files/actividad/<?php echo $entrada['actividad_id'] ?>/portada.jpg)" style="min-height:60rem;">
        <h2><?php echo $entrada['actividad_nombre']?></h2>
    </div>



    <hr>
</div>

<?php } ?>

<?php include "footer.php";?>

<script>
    $('#carousel-inner div:first-child').addClass('active');
</script>