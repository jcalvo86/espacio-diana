<?php   
    include 'inc/template/navbar.php';
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index_main.php">Inicio</a></li>
    <li class="breadcrumb-item">Login</li>
  </ol>
</nav>

<div class="contenedor">
  <div class="login__container">
    <form class="" id="login" style="display: flex; flex-flow:column;">
        <div class="input-field login__field">
            <input  id="login_user" type="text" class="validate  login__input" placeholder="Email">
        </div>
        <div class="input-field login__field">
            <input id="login_password" type="password" class="validate login__input" placeholder="Password">
        </div>

        <button class="btn waves-effect waves-light" type="submit" name="action">Log in
            <i class="fas fa-arrow-right"></i>
        </button>
    </form>
  </div>
</div>



<?php include "footer.php";?>