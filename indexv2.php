<?php
  include 'inc/template/navbar.php';
  include 'inc/php/pregunta_list.php';

  $listado = get_preguntas_list();
?>

  <div class="contenedor">
    <div class="graffiti_fondo">
      <?php for( $i = 0; $i <= 11; $i++){?>
        <h1 class="item" id="word<?php echo $i; ?>"><?php echo $preguntas[$i]; ?></h1>
      <?php }?>
    </div>

      <div class="pregunta_container" style=>
      <h1><?php echo($pregunta_last['preg_nombre']);?> quiere <?php echo($pregunta_last['preg_texto']);?><?php ?></h1>
        <h1>¿Qué te gustaría que pasara aquí?</h1>
        <form id="pregunta_form" action="pregunta" style="    width: 80%;">

          <div class="input-group">
            <textarea id="pregunta_respuesta" class="form-control" aria-label="With textarea" placeholder="Cuéntanos que quieres que pase en Diana" data-container="body" data-toggle="popover" data-placement="bottom" data-content="¿Seguro que no te interesa que pasen cosas en este lugar?"></textarea>
          </div>
          <br>
          
          <div class="form-group">
            <input type="text" class="form-control" id="pregunta_usuario" aria-describedby="emailHelp" placeholder="Hola, me llamo..." data-container="body" data-toggle="popover" data-placement="bottom" data-content="Mi nombre es Diana, ¿Y el tuyo?.">
            <small  class="form-text text-muted">No compartiremos tu información.</small>
          </div>

          <button type="submit" class="btn btn-secondary">Enviar</button>
        </form>
      </div>

  </div>
</body>
</html>   


<div id="modalNewsletter" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Gracias <span id="nombre_usuario"></span> </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3>Estamos buscando dar vida a este espacio, con todas nuestras ideas.</h3>
        <p>Si quieres que te informemos de nuestras novedades, puedes inscribirte en nuestro newsletter.</p>
        <form id="updateEmail_form">
          <div class="form-group">
            <input type="text" id="id_usuario" hidden>
            <input type="email" class="form-control" id="email_usuario" aria-describedby="emailHelp" placeholder="Ingresa tu email">
            <small id="emailHelp" class="form-text text-muted">No compartiremos tu información.</small>
          </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Quiero estar al tanto</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div id="modalSuccess" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h3>Tu email ha sido guardado</h3>
        <p>Pronto recibirás más noticias de lo que está sucediendo en Diana.</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script
  src="https://code.jquery.com/jquery-3.5.1.js"
  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="inc/js/listener.js"></script>

<script>

$(function () {
  count = 0;
  wordsArray = <?php echo json_encode($preguntas );?>;

 duration = 6000;
 interval = 2000;

  setInterval(function () {
    count++;
    $("#word0").fadeOut(interval, function () {
      $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(interval);
    });
  }, Math.floor(Math.random() * interval + duration));

  setInterval(function () {
    count++;
    $("#word1").fadeOut(interval, function () {
      $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(interval);
    });
  }, Math.floor(Math.random() * interval + duration));

  setInterval(function () {
    count++;
    $("#word2").fadeOut(interval, function () {
      $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(interval);
    });
  }, Math.floor(Math.random() * interval + duration));

  setInterval(function () {
    count++;
    $("#word3").fadeOut(interval, function () {
      $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(interval);
    });
  }, Math.floor(Math.random() * interval + duration));

  setInterval(function () {
    count++;
    $("#word4").fadeOut(interval, function () {
      $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(interval);
    });
  }, Math.floor(Math.random() * interval + duration));

  setInterval(function () {
    count++;
    $("#word5").fadeOut(interval, function () {
      $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(interval);
    });
  }, Math.floor(Math.random() * interval + duration));

  setInterval(function () {
    count++;
    $("#word6").fadeOut(interval, function () {
      $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(interval);
    });
  }, Math.floor(Math.random() * interval + duration));

  setInterval(function () {
    count++;
    $("#word7").fadeOut(interval, function () {
      $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(interval);
    });
  }, Math.floor(Math.random() * interval + duration));

  setInterval(function () {
    count++;
    $("#word8").fadeOut(interval, function () {
      $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(interval);
    });
  }, Math.floor(Math.random() * interval + duration));

  setInterval(function () {
    count++;
    $("#word9").fadeOut(interval, function () {
      $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(interval);
    });
  }, Math.floor(Math.random() * interval + duration));

  setInterval(function () {
    count++;
    $("#word10").fadeOut(interval, function () {
      $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(interval);
    });
  }, Math.floor(Math.random() * interval + duration));

  setInterval(function () {
    count++;
    $("#word11").fadeOut(interval, function () {
      $(this).text(wordsArray[Math.floor(Math.random() * wordsArray.length)]).fadeIn(interval);
    });
  }, Math.floor(Math.random() * interval + duration));
});

</script>