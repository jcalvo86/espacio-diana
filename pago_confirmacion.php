<?php
require_once './vendor/autoload.php';

use Transbank\Webpay\Webpay;
use Transbank\Webpay\WebpayNormal;
use Transbank\Webpay\Configuration;

$configuration = new Configuration();

$configuration = Configuration::forTestingWebpayPlusNormal();

// $configuration->setEnvironment("PRODUCCION");

// $configuration->setCommerceCode("597034054207");

// $configuration->setPublicCert(
//     "-----BEGIN CERTIFICATE-----\n" .
//     "MIIDWjCCAkICCQDlefEvKVhpezANBgkqhkiG9w0BAQsFADBvMQswCQYDVQQGEwJD\n" .
//     "TDETMBEGA1UECAwKU29tZS1TdGF0ZTERMA8GA1UEBwwIU2FudGlhZ28xITAfBgNV\n" .
//     "BAoMGEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDEVMBMGA1UEAwwMNTk3MDM0MDU0\n" .
//     "MjA3MB4XDTIwMTAyOTE2MjcyNloXDTI0MTAyODE2MjcyNlowbzELMAkGA1UEBhMC\n" .
//     "Q0wxEzARBgNVBAgMClNvbWUtU3RhdGUxETAPBgNVBAcMCFNhbnRpYWdvMSEwHwYD\n" .
//     "VQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQxFTATBgNVBAMMDDU5NzAzNDA1\n" .
//     "NDIwNzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALl6qpclmVHF6J2N\n" .
//     "AHiRXDgXi9qZ91i2E/5y7PYdxSEG80pjnNf0fu6Z0ult9SiKxoThptWk78aEWXqe\n" .
//     "yz9OdzBnQpi/H+/phQfainXXK5EQ/fU/ltgYZwr255dku+dBLmmvSmELd33uYWRD\n" .
//     "wCG5r2vLqAXhHJQjjzNdvRwMewsqALEL8FUkwDmzqsfjDq1pxa8gHdOvJeTggaKg\n" .
//     "7SemRUyrbpHJ7NO8MV2F2sgH6Cv9kwYN53yW/OJLClhD4XzGZwvcnyYVCJiYO34z\n" .
//     "vGukMuDO6dzDgHj7zwkRx0XjRZCBVMJOf/OqKpsx/255BcU9LJKb6M2cBYzfu/XX\n" .
//     "jLFYaW8CAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAX/RM9QD5+HEJixZpYmxUlXtx\n" .
//     "W/RWh1dccJ3QJ/n9nP1WiaOQHA4igscB3y5hpbtjZvrwVnD2Qc9qoP5WCnCeufik\n" .
//     "fYItfW8jGHrHrhf4Bh/nIzmDkXKR+Gmrn2iD6WE5KUmN8jDi4FDjDrya1gnTp9qI\n" .
//     "5wGUmlchnY0k3d+Feix0VQ4Tc+kExHB+3DJdymoZC8f7h4KzX/KdlcPEMwU5f17h\n" .
//     "bVY1Er6ofytS1h/lYmD9eVFoZr52W4mL548H1F+W8TTyMPjXT+WlTr0bb8kKYRai\n" .
//     "hymYhGiTXScFpcW3VkFqIkl2yHD7f7phaqPE/D3/XNq5Ea6Jqn7SJYZ7OH0FSA==\n" .
//     "-----END CERTIFICATE-----\n"
// );

// $configuration->setPrivateKey(
//     "-----BEGIN RSA PRIVATE KEY-----\n" .
// "MIIEpAIBAAKCAQEAuXqqlyWZUcXonY0AeJFcOBeL2pn3WLYT/nLs9h3FIQbzSmOc\n" .
// "1/R+7pnS6W31KIrGhOGm1aTvxoRZep7LP053MGdCmL8f7+mFB9qKddcrkRD99T+W\n" .
// "2BhnCvbnl2S750Euaa9KYQt3fe5hZEPAIbmva8uoBeEclCOPM129HAx7CyoAsQvw\n" .
// "VSTAObOqx+MOrWnFryAd068l5OCBoqDtJ6ZFTKtukcns07wxXYXayAfoK/2TBg3n\n" .
// "fJb84ksKWEPhfMZnC9yfJhUImJg7fjO8a6Qy4M7p3MOAePvPCRHHReNFkIFUwk5/\n" .
// "86oqmzH/bnkFxT0skpvozZwFjN+79deMsVhpbwIDAQABAoIBAALju1ahelMAz0/t\n" .
// "C23Vbeddzor4TRcvtv4G0lQ/QHv0PMQS6zWeFIApG1URpnkcOF2Hmm7HpAimtTIo\n" .
// "2SfCVC8H3My+ZYP6Ul099VxCnuYiqqIwfGwfbTOJhzAfnKHMrSluVq1OXW0z0oaa\n" .
// "W5rq5qW6RYLZ7/UK3MdDFZgjRaN13ENyJULSo/oejrU8FUVKg3MU2ugA4Csn+pKd\n" .
// "75pT+HrzTC9pmok9HcyvZikMqdZF/NSB6smhxsB94kGkjlXNdx4xf4tfRiy1xXfk\n" .
// "rz3liV/zwI0boizltAQVkI4rGlg1257RJpUu8j421sHVJFBbmlNaR3a6GalyUkB2\n" .
// "MN7EJ/ECgYEA4o6SN04An2V1vgFLpA47Ku9KHw/K+YEf/O7Ng2R24mzOp1J3MCoz\n" .
// "LDDP6UqJ3phJlmRbzbK8/zoA70OlH3G05n/OjYnp5ozvY1lZuacTqgdbqUQ+hyPB\n" .
// "ghSGWE52yTthShsNUv9uSR4EgptOlsAf/juv9GjIFy67oEMF0Q38sTcCgYEA0ZV1\n" .
// "yNUE1iudP1HNMp069J0ofWvwKmA0bPvo69PXLPyAeVdyV6feAndhO9iNnIX05rGO\n" .
// "K/RVwepS8dP6SBiodtqhkQQEoizFIfGp1f3Jok1B+22Y9jDm0+dcYr9YTF0ZEEBC\n" .
// "lGx0MG4c8kBCt9vDf1aE8uNOsY2IunfJH/FUhYkCgYEAzz8alErfmPBoT+LBfhai\n" .
// "m3kANf/tbmZni6osB6jsc96TAE8Yxs+jtiYE22UbT6vBjTjLHhUGfPdjiXw43dXX\n" .
// "ZQ+/NIzXkdaU2i4PPRyQiXZnrCTIijj0OHlsF/XXBegy7GZovub4zRwPhiZqMs8X\n" .
// "tSqGoC8F8ucGLemBtaZ05OsCgYEAl/30NXmG0GtA1rLURWiRYOS4gHzO52xtmLv3\n" .
// "507yNcXIxjcMzVUIRHGfKm+aA3GCcetCV84Sg1cUYByC3dZEFi3oDesEETi2ni9M\n" .
// "I8yxderhdx28WI6OWA7piROLTZYWnxp7LniYqqawh17jA8N0u9xa/mqqn0ktZ02H\n" .
// "poq16UECgYB7CnR5N9nV0VyOVp7jGt+Hka02bWMSFWcpYHi1p6oCjDuptBSw1cu+\n" .
// "TthKS/WrrfNbLec0+8bF1eeVj8dZ1BPgrny11foczpFhGjrxGn2TXGXL3qp0sFdL\n" .
// "CJ2jW6/UjnOrNjBuO6Uwn/pv1KvQ1slQklXgV2Kez+TjhXYCmE9jCg==\n" .
// "-----END RSA PRIVATE KEY-----\n"
// );

$transaction = (new Webpay($configuration))-> getNormalTransaction();

include 'inc/php/actividades_list.php';

$post= $_POST;
// print_r($post);

$funcion = getFuncionInfo($post['funcion']);

$returnUrl = 'https://www.espaciodiana.cl/return.php';
$finalUrl = 'https://www.espaciodiana.cl/final.php';

$emailin = $post['email'];
$funcionin = $post['funcion'];

if($post['tipoEntrada'] == "entrada"){
    $cantidadin = $post['entradas'];
    $amount = $post['valor'] * $post['entradas'];
    $entradas = $post['entradas'];

}else if($post['tipoEntrada'] == "gorraon"){
    $cantidadin = 1;
    $amount = $post['valor'];
    if($amount == 'otro'){
        $amount = $post['entradasgorraonline_input'];
    }
    
    $entradas = 1;
}

$nombre_nino = '-';
if(array_key_exists('nombre_nino',$post)){
    $nombre_nino = $post['nombre_nino'];
}

$telefono = 00000;
if(array_key_exists('telefono',$post)){
    $telefono = $post['telefono'];
}

$sessionId = 'sessionId';
$buyOrder = strval(rand(10000, 99999));

$initResult = $transaction->initTransaction($amount, $buyOrder, $sessionId, $returnUrl, $finalUrl);
$formAction = $initResult->url;
$tokenWs  = $initResult->token;

for($i = 0; $i < $cantidadin; $i++){
    $codigo = strval(rand(100000, 999999));
    $nombrein = $post['nombre'.$i];

    include 'inc/php/connection.php';
    $stmt = $conn->query("INSERT INTO entradas (
        entrada_cantidad, entrada_nombre, entrada_email, entrada_funcion_id, orden_token, entrada_code, entrada_pagado, orden_aceptada, taller_nino, taller_fono)
        VALUES (
        '$cantidadin','$nombrein','$emailin','$funcionin', '$tokenWs', '$codigo', '$amount' , 0, '$nombre_nino', '$telefono'
        )"
      );
      
      $conn->close();
}

include 'inc/template/navbar.php';

?>

<style>
    body{
        font-size:16px;
    }
</style>

<div class="contenedor">
    <div class="segment_cabecera background-image" style="background-image: url('files/imagenes/img_0001_diana002.jpg');">

        <div class="segment_cabecera_contenedor">
            <div class="segment_cabecera_titulo">
                <h1>Boletería</h1>
            </div>
        </div>
    </div>
</div>

<div class="segment_boleteria flex-col layout-box">
<h2><?php echo $funcion['actividad_nombre'];?></h2>

<hr>
<div class="flex-row" style="width: 100%;">
    <div class="container flex-1">
        <div class="fecha flex-col">
            <div class="flex-row">
                <div class="fecha_fecha">
                <p><?php echo displayFecha($funcion['funcion_dia']);?> / <?php echo displayHora($funcion['funcion_hora']);?></p>
                </div>
            </div>
        </div>
        <hr>

        <?php if($post['tipoEntrada']=="entrada"){ ?>
            <p><?php echo $entradas;?> entradas.</p>

        <?php }else if($post['tipoEntrada']=="gorraon"){ ?>
            <p>Función a la gorra. </p>
            <p>Puede realizar un aporte desde $1.000.</p>

        <?php } ?>
        

    </div>

    <form action="<?php echo $formAction ?>" method="POST" class="form-inline flex-1 flex-col" role="form" style="">

        <p><?php echo $post['nombre0'];?></p>

        <p><?php echo $post['email'];?></p>


        <hr>
        <p>Total: $<?php echo $amount;?> CLP</p>
        <p>Orden de Compra: <?php echo $buyOrder;?></p>

        <input type="hidden" name="token_ws" value="<?php echo $tokenWs ?>">
        <input type="hidden" name="funcion" value="<?php $post['id'] ?>">
        <input type="hidden" name="entradas" value="<?php $entradas ?>">
        <input type="hidden" name="nombre" value="<?php $post['nombre'] ?>">
        <input type="hidden" name="email" value="<?php $post['email'] ?>">
        <button type="submit" class="btn btn-red btn-pago" style="width: 60%;  margin-left:20%;">Pagar</button>
    </form>
</div>

</div>


<?php include "footer.php";?>

<script>
    $('#carousel-inner div:first-child').addClass('active');
</script>