<?php

require_once './vendor/autoload.php';

use Transbank\Webpay\Webpay;
use Transbank\Webpay\Configuration;

$configuration = Configuration::forTestingWebpayPlusNormal();
$transaction = (new Webpay($configuration))-> getNormalTransaction();

$tokenWs = filter_input(INPUT_POST, 'token_ws');
$result = $transaction->getTransactionResult($tokenWs);

// echo $tokenWs;
// print_r($result);

include 'inc/template/navbar.php';
include 'inc/php/actividades_list.php';

$entradas = getEntradaListPorToken($tokenWs);

$entrada_title="Hubo un error en la compra";
$displayCorrect = "hidden";
$displayError = "";
$contacto = "";

$orden_aceptada = 1;
foreach($entradas as $key => $entrada){
    if($key == 0){
        $contacto = $entrada;
    }

    if($entrada['orden_aceptada'] == 0){
        $orden_aceptada = 0;
    }
}

if($orden_aceptada == 1){
    $entrada_title = "Entradas adquiridas";
    $displayCorrect="";
    $displayError = "hidden";
}
?>

<style>
    body{
        font-size:16px;
    }
</style>

<div class="contenedor">
    <div class="segment_cabecera background-image" style="background-image: url('files/imagenes/img_0001_diana002.jpg'); max-height:200px; min-height:0px;">

        <div class="segment_cabecera_contenedor">
            <div class="segment_cabecera_titulo">
                <h1><?php echo $entrada_title?></h1>
            </div>
        </div>
    </div>
</div>

<div class="segment_boleteria flex-row layout-box" <?php echo $displayCorrect;?>>
    <div class='col-8'>
        <h4><?php echo $contacto['entrada_nombre']?> tus entradas para "<?php echo $contacto['actividad_nombre']?>" en su función del día <?php echo displayFecha($contacto['funcion_dia']);?> a las <?php echo $contacto['funcion_hora']?> horas, fueron adquiridas correctamente.</h4>
        <br>

        <div class="flex-row">
            <div class="col" style="padding-left:0;">
                <h4>Fecha:</h4>
                <p><?php echo displayFecha($contacto['funcion_dia']);?></p>
            </div>

            <div class="col">
                <h4>Hora:</h4>
                <p><?php echo $contacto['funcion_hora']?></p>
            </div>
        </div>

        <h4>Asistentes:</h4>
        <table class="table">
            <thead>
                <tr><th scope="col">Nombre</th><th scope="col">Código</th></tr>
            </thead>
            <tbody>
            <?php
            foreach($entradas as $entrada){?>
                <tr><td><?php echo$entrada['entrada_nombre']?></td><td><?php echo$entrada['entrada_code']?></td></tr>
            <?php } ?>
            </tbody>
        </table>

        <p  class="small">Al momento del pago se genera un mail con un nombre de usuario y una clave de acceso que llegara al correo ingresado y podrá ser usada una sola vez (revisar carpeta spam).</p>
        <p  class="small">El usuario podrá disfrutar de la función, ingresando a la plataforma el día de la función desde las <?php echo $contacto['funcion_habilitado'];?> hasta las <?php echo $contacto['funcion_fin'];?> horas. </p>

<br>
        <a href="funcion.php?funcion_id=<?php echo $contacto['funcion_id'] ?>" class="btn btn-rojo btn-fullwidth">
            Ver Obra
        </a>

<br>

        <p class="small">Para visualizar la obra, deberá ingresar al link de la obra, e ingresar el nombre del asistente junto con su código personal.</p>
        <hr>

        <div class="flex-col flex-2">
            <h4><?php echo $entrada['actividad_nombre']?></h4>
            <p><?php echo $entrada['actividad_descripcion']?></p>
        </div>

        <div class="encuesta">
        </div>
    </div>


    <div class="col-4 background-image" style="background-image:url(files/actividad/<?php echo $entrada['actividad_id'] ?>/portada.jpg)" style="min-height:60rem;">
        <h2><?php echo $entrada['actividad_nombre']?></h2>
    </div>



    <hr>
</div>

<div class="segment_boleteria flex-col layout-box" <?php echo $displayError;?>>
    <div class="flex-row">
        <div class="flex-col flex-1 backgound-image" style="background-image:url(files/actividades/<?php echo $entrada['actividad_id'] ?>/portada.jpg)">
            <h2>Tus entradas lamentablementes no pudieron ser adquiridas.</h2>
        </div>

        <div class="flex-col flex-2">
            <p>Por algún motivo tus entradas no pudieron ser adquiridas, en caso de alguna duda contáctate con contacto@espaciodiana.cl o prueba intentarlo nuevamente.</p>
            <p>Si tienes el comprobante de transferencia y has recibido este mensaje, comunícate con nosotros a <b>contacto@espaciodiana.cl</b> para solucionar este malentendido.</p>
        </div>
    </div>

</div>


<?php include "footer.php";?>

<script>
    console.log(localStorage);
    $('#carousel-inner div:first-child').addClass('active');
</script>