<script type="text/javascript">
document.cookie = 'window_width='+window.innerWidth;</script>

<?php

  if (isset($_GET['end_session'])) {
      $_SESSION = array();
  }
  
  include 'inc/php/functions.php';
  include 'inc/php/connection.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MKLNHFR');</script>
<!-- End Google Tag Manager -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    <link type="text/css" rel="stylesheet" href="inc/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet"> 

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Espacio Diana</title>
    
</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MKLNHFR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="segment_resumen_descripcion" style="display:flex; flex-flow:column; height:100vh; justify-content:center;">
    <!--<h1>Cargando</h1>-->
    <img src="files/diana_preloader.gif" alt="" style="width:100px;">
  </div>

  <script>
        window.setTimeout(function () {location.href = "index_main.php";}, 5000);
  </script>
</body>