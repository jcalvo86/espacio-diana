<?php   
    include 'inc/template/navbar.php';
    include 'inc/php/actividades_list.php';


    if (isset($_GET['funcion_id'])) {
        $id = $_GET['funcion_id'];
    }

    $funcion = getFuncionInfo($id);
    
    // print_r($funcion);

    $locked = true;
    $message = "No tiene acceso a esta función.";

    //CALCULO DE DIAS

    date_default_timezone_set('America/Santiago');
    
    $dia = time();
    $your_date = strtotime( $funcion['funcion_dia']);
    $datediff = $your_date - $dia;

    $dias_restantes = (round($datediff / (60 * 60 * 24)));

    $NowTime = date("H:i");
    
    $now = strtotime($NowTime);

    $inicio_obra = strtotime($funcion['funcion_habilitado']);

    $counter = ($now - $inicio_obra);

    $duracion = $funcion['funcion_duracion'] * 60;
    $fin_obra = $inicio_obra + $duracion;

    $total_min = round(($inicio_obra - $now)/60);
    
    $left_hour = floor($total_min/60);
    $left_minutes = $total_min - (60*$left_hour);

    $display = "hidden";

    $message = "";
    //  $message .= $datediff. " // ".$dias_restantes ." //";

  if($dias_restantes > 0){
    if($dias_restantes == 1){
      $message .= "Esta función se visualizará el día de mañana.";
    }else{
      $message .= "Faltan ".($dias_restantes)." días para esta función";
    }

  }else if($dias_restantes < -1){
    $message .= "Esta función ya fue presentada.";

  }else{
    $locked_fecha = 1;
    if($total_min > 0){
      $message .= "Esta función está programada para hoy a las ". $funcion['funcion_hora'] .".<br>";
      $message .= " Quedan ". $left_hour. " horas con ". $left_minutes . " minutos para habilitar la transmisión.";
    }else if($total_min < -$duracion){
      $message .= $total_min ." Esta función ya terminó.";
    }else{
      $message .= "<h4> En este momento usted puede ingresar a ver la función con el código entregado.</h4>";
      $message .= "<p> Esta función estará disponible por ".($total_min + $duracion)." minutos.</p>";
      $display = "";
    }
  }

  if($funcion['funcion_pago']== "archivo"){
    $message = "Esta función es de archivo";
    $display = "";
  }
?>

  <div class="modal" tabindex="-1" role="dialog" id="funcionModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><?php echo text_capitalize($funcion['actividad_nombre'])?></h5>
        </div>
        <div class="modal-body">
          <p><?php echo($message); ?></p>

          <form id="validarEntrada_form">
            <div class="form-row" <?php echo $display?>>
              <!-- <div class="col-md-6 mb-3">
                <label for="validationCustom01">Nombre</label>
                <input type="text" class="form-control" id="user_nombre" placeholder="Diana Prince">
                <div id="user_nombre_valid" class="display-hide"></div>
              </div> -->
              <div class="col-md-6 mb-3" <?php echo $display?>>
                <label for="validationCustom02">Código</label>
                <input type="text" class="form-control" id="user_codigo" placeholder="xxxxxx">
                <div  id="user_codigo_valid" class="display-hide"></div>
              </div>
              <div  id="user_valid" class="display-hide"></div>
            </div>

        </div>
        <div class="modal-footer">
            <input type="text" id="funcion_id" value="<?php echo $id?>" hidden>
            <button class="btn btn-rojo" type="submit" <?php echo $display?>>Ingresar</button>
            <a href="cartelera.php" class="btn btn-primary">Volver</a>
          </form>
        </div>
      </div>
    </div>
  </div>

  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index_main.php">Inicio</a></li>
      <li class="breadcrumb-item"><a href="cartelera.php">Cartelera</a></li>
      <li class="breadcrumb-item active" aria-current="page"><?php echo text_capitalize($funcion['actividad_nombre'])?></li>
    </ol>
  </nav>

  <div class="contenedor" style="">

    <div class="segment_actividad " style="display: flex; min-height: 50vw;">
      <div class="col layout-box-small">
        <h3><?php echo text_capitalize($funcion['actividad_nombre'])?></h3>

        <p><?php echo text_capitalize($funcion['actividad_descripcion'])?></p>
        <hr>

        <div class="video_container">
          <iframe src="https://player.vimeo.com/video/<?php echo $funcion['actividad_link']?>" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        </div>
        

      </div>
    </div>
  </div>

  <div class="segment_actividad_comentarios">
      <!-- <div class="segment_actividad_comentarios">
          <p>
              "Una obra que sustituye a las seis mujeres que protagonizan la obra por seis “hombres”, quienes tensionan la idea de género, sexo y rol social a desempeñar."
          </p>
          <h4>
              Pedro Pérez
          </h4>
      </div>

      <hr>

      <div class="segment_actividad_comentarios">
          <p>
              "Una obra que sustituye a las seis mujeres que protagonizan la obra por seis “hombres”, quienes tensionan la idea de género, sexo y rol social a desempeñar."
          </p>
          <h4>
              Pedro Pérez
          </h4>
      </div>
      <hr> -->
  </div>

<?php include "footer.php";?>
<script src="inc/js/functions.js"></script>
<script src="inc/js/listener.js"></script>

<script>
  $('#funcionModal').modal({
    backdrop: 'static'
  })
  $('#funcionModal').modal('show');

  // var timerID = setInterval(function(){revalidar_entrada()}, 20 * 1000); // 60 * 1000 milsec
</script>