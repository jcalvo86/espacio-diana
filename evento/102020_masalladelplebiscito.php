<?php include '../inc/template/navbar_secondlvl.php'; ?>

<div class="contenedor">
    <div class="segment_transmision box">
        <div class="segment_transmision_video">
            <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fartesmenores.org%2Fvideos%2F1073947946395045%2F&amp;"  style="border:none;overflow:hidden width: 100%" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true"></iframe>
        </div>

        <div class="descripcion">
            <h4>Más alla del plebiscito</h4>
        </div>
    </div>

    <div class="segment_invitados box background_grisoscuro">
        <h3>Invitados:</h3>
        <div class="invitado">
            <div class="invitado_imagen">
                <img src="../files/events/10-2020/atria.jpg" alt="">
            </div>
            <div class="invitado_info">
                <div class="invitado_nombre">
                    <h4>Fernando Atrias</h4>
                </div>
            </div>
        </div>
        
        <hr>

        <div class="invitado">
            <div class="invitado_imagen">
                <img src="../files/events/10-2020/andreagutierrez.jpg" alt="">
            </div>
            <div class="invitado_info">
                <div class="invitado_nombre">
                    <h4>Andrea Gutierrez</h4>
                </div>
                <div class="invitado_descripcion">
                   <p> </p>
                </div>
            </div>
        </div>

        <hr>

        <div class="invitado">
            <div class="invitado_imagen">
                <img src="../files/events/10-2020/masalladelplebiscito_cuadrado.jpeg" alt="">
            </div>
            <div class="invitado_info">
                <div class="invitado_nombre">
                    <h4>Espacio Diana</h4>
                </div>
            </div>
        </div>
    </div>

</div>

<?php include "footer.php" ?>