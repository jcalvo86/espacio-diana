<?php   
    include 'inc/template/navbar.php';
?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="index_main.php">Inicio</a></li>
    <li class="breadcrumb-item active" aria-current="page">Servicios</li>
  </ol>
</nav>

<div class="contenedor">
    <div class="segment_cabecera background-image" style="background-image: url('files/imagenes/img_0001_diana002.jpg');">

        <div class="segment_cabecera_contenedor">
            <div class="segment_cabecera_titulo">
                <h1>ESPACIO DIANA</h1>
            </div>
        </div>
    </div>


    <div class="segment_historia flex-row">
        <div class="fondo">
            <div class="flex-3"></div>
            <div class="flex-2 background-rojo"></div>
        </div>

        <div class="flex-col flex-1 layout-box-left historia_prev_content">
            <h2>Servicios</h2>

            <ul>
                <li>Arriendo de espacios y salas.</li>
                <li>Registro audiovisual de obras acorde a plan Covid.</li>
                <li>Funciones Privadas</li>
                <li>Actividades educativas.</li>
                <li>Convenios.</li>
            </ul>
        </div>
        
        <div class="flex-col layout-box-right flex-1">
            <div class="block-image-big background-image" style="background-image:linear-gradient(rgba(0,0,0,0),rgba(0,0,0,1)),url(files/imagenes/img_0002_diana003.jpg)">
                <p>Conciertos</p>
            </div>
            <div class="block-image-big background-image" style="background-image:linear-gradient(rgba(0,0,0,0),rgba(0,0,0,1)),url(files/imagenes/img_0003_diana004.jpg)">
                <p>Familiar</p>
            </div>
        </div>
    </div>
</div>

<?php include "footer.php";?>
