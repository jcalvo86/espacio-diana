<!DOCTYPE html>
<html lang="es">
<head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-XE1W2CEM6V"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-XE1W2CEM6V');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MKLNHFR');</script>
<!-- End Google Tag Manager -->

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Espacio Diana</title>
    <meta name="description" content="Lugar destinado a la cultura, ocio y entretención, situado en el corazón del Barrio San Diego, en un edificio patrimonial de principios del siglo XX y emplazado detrás del tradicional parque de entretenciones Juegos Diana"/>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.8.1/css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.8.1/css/react-select.css" />
</head>

<style>
.contenedor{
    display:flex;
}

.item{
    height: 300px;
    display: flex;
    flex-flow: column;
    justify-content: space-around;
    background-color: red;
    align-items: center;
}
}

.item1{

}

.item2{

}
</style>

<body>
<div class="body__container">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MKLNHFR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<nav class="navbar navbar-expand-lg navbar-light bg-light">

    <div class="flex-row">
        <a href="https://www.facebook.com/espaciodiana/"><i class="fab fa-facebook-f fa-1x" style="margin-left:10px;"></i></a>
        <a href="https://www.instagram.com/espaciodiana/"><i class="fab fa-instagram fa-1x" style="margin-left:10px;"></i></a>
        <a href="https://twitter.com/espacio_diana"><i class="fab fa-twitter fa-1x" style="margin-left:10px;"></i></a>
    </div>

    <!-- Icono de home -->
    <a class="navbar-brand" href="#">
        <img src="files/logo.png" width="30" height="30" alt="" loading="lazy">
        <img src="files/logo.png" style="width:30px" height="30" alt="" loading="lazy">
    </a>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    <a class="navbar-brand" href="#">Hidden brand</a>

    <!-- Elementos del Menu -->
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="#">Obras <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Talleres</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Talleres</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Talleres</a>
      </li>

    </ul>

    <!-- Formulario de búsqueda -->
    <!-- <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form> -->
  </div>

</nav>


